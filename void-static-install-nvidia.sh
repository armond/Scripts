#!/bin/bash

set -euo pipefail

# Configuration variables (constants)
readonly drive='nvme0n1'
readonly timezone='Africa/Luanda'
readonly mirror='https://repo-de.voidlinux.org'
readonly kernel='linux6.12'
readonly font='2407.24'
readonly localization='en_US.UTF-8'
readonly hostname='lenovo'
readonly swapsize='4G'
readonly boot_options='quiet loglevel=3 console=tty2 nmi_watchdog=0 nvidia_drm.modeset=1 nvidia_drm.fbdev=1'
readonly config_directory='Configurations/home'

# Package lists (constants)
readonly install_packages=(
  'base-devel' 'bash-completion' 'chrony' 'efibootmgr' 'fuse' 'fwupd' 'linux-tools' 'lz4' 'ngetty' 'parted' 'psmisc' 'snooze' 'socklog-void' 'strace' 'systemd-boot' 'wget' 'xdg-user-dirs' 'xtools'
  'xorg-minimal' 'xorg-apps' 'xf86-video-amdgpu' 'mesa-vulkan-radeon' 'mesa-vaapi' 'mesa-vdpau' 'nftables' 'wireguard-tools'
  'pipewire' 'alsa-pipewire' 'gstreamer1-pipewire' 'libjack-pipewire' 'libspa-bluetooth' 'libspa-jack' 'libspa-v4l2' 'rtkit'
  'kde-plasma' 'kde-baseapps' 'ark' 'gwenview' 'kcalc' 'kdiff3' 'konversation' 'krusader' 'ksystemlog' 'okular' 'spectacle' 'bluez' 'NetworkManager'
  'bandwhich' 'ghostty' 'htop' 'lutris' 'neovim' 'wl-clipboard' 'xsel' 'nvtop' 'weechat'
  'chromium' 'firefox' 'qutebrowser' 'python3-adblock' 'mpv' 'yt-dlp' 'qbittorrent' 'telegram-desktop'
  'nerd-fonts-symbols-ttf' 'noto-fonts-cjk' 'noto-fonts-emoji' 'noto-fonts-ttf' 'noto-fonts-ttf-extra'
)

readonly nonfree_packages=(
  'libgcc-32bit' 'libstdc++-32bit' 'libdrm-32bit' 'libglvnd-32bit' 'mesa-dri-32bit' 'vulkan-loader-32bit' 'mesa-vulkan-radeon-32bit' 'mesa-vaapi-32bit' 'mesa-vdpau-32bit'
  'nvidia' 'nvidia-libs-32bit'
  'pipewire-32bit' 'alsa-pipewire-32bit' 'gstreamer1-pipewire-32bit' 'libjack-pipewire-32bit' 'libspa-bluetooth-32bit' 'libspa-jack-32bit' 'libspa-v4l2-32bit'
  'steam' 'gamemode' 'libgamemode-32bit'
  'wine' 'wine-32bit' 'wine-gecko' 'wine-mono' 'winetricks'
)

# Functions
setup_partitions() {
  echo "Setting up partitions on /dev/${drive}..."
  wipefs -fa "/dev/${drive}" > /dev/null
  parted -s "/dev/${drive}" mklabel gpt \
    mkpart ESP fat32 1MiB 1025MiB \
    mkpart primary ext4 1025MiB 100% \
    set 1 esp on
  mkfs.fat -F 32 "/dev/${drive}p1" > /dev/null
  mkfs.ext4 -F "/dev/${drive}p2" > /dev/null 2>&1
  mount "/dev/${drive}p2" /mnt
  mount --mkdir "/dev/${drive}p1" /mnt/boot
  echo "Partitions set up successfully."
}

install_base_system() {
  echo "Installing base system..."
  if ! curl -LO "${mirror}/static/xbps-static-latest.x86_64-musl.tar.xz"; then
    echo "Failed to download xbps-static-latest.x86_64-musl.tar.xz"
    exit 1
  fi
  tar xf xbps-static-latest.x86_64-musl.tar.xz
  mkdir -p /mnt/var/db/xbps/keys
  cp ./var/db/xbps/keys/* /mnt/var/db/xbps/keys/
  XBPS_ARCH=x86_64 ./usr/bin/xbps-install.static -Sy -R "${mirror}/current" -r /mnt "${kernel}" "${kernel}-headers" base-system
  echo "Base system installed successfully."
}

configure_fstab() {
  local partuuid1 partuuid2
  partuuid1=$(blkid -o value -s PARTUUID "/dev/${drive}p1")
  partuuid2=$(blkid -o value -s PARTUUID "/dev/${drive}p2")
  echo "PARTUUID=${partuuid1} /boot vfat defaults 0 2" >> /mnt/etc/fstab
  echo "PARTUUID=${partuuid2} /     ext4 defaults 0 1" >> /mnt/etc/fstab
}

chroot_setup() {
  local user="${1}"
  local userpassword="${2}"
  local rootpassword="${3}"

  for dir in dev proc sys run; do
    mkdir -p "/mnt/${dir}"
    mount --rbind "/${dir}" "/mnt/${dir}"
    mount --make-rslave "/mnt/${dir}"
  done
  cp --dereference /etc/resolv.conf /mnt/etc/
  cp "$0" /mnt/setup
  chroot /mnt /bin/bash ./setup "${user}" "${userpassword}" "${rootpassword}"
  rm -f /mnt/setup
  umount -R /mnt
}

configure_system() {
  local user="${1}"
  local userpassword="${2}"
  local rootpassword="${3}"

  xbps-pkgdb -m manual "${kernel}" "${kernel}-headers" linux-base

  mkdir -p /etc/xbps.d
  echo 'ignorepkg=linux' > /etc/xbps.d/99-ignore.conf
  echo 'ignorepkg=linux-headers' >> /etc/xbps.d/99-ignore.conf
  xbps-remove -Ry linux

  cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
  sed -i "s|https://repo-default.voidlinux.org|${mirror}|g" /etc/xbps.d/*-repository-*.conf

  xbps-install -Sy
  xbps-install -y "${install_packages[@]}"
  xbps-install -y void-repo-nonfree void-repo-multilib{,-nonfree}

  cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
  sed -i "s|https://repo-default.voidlinux.org|${mirror}|g" /etc/xbps.d/*-repository-*.conf

  xbps-install -y "${nonfree_packages[@]}"

  echo "LANG=${localization}" > /etc/locale.conf
  sed -i "/${localization}/s/^#//g" /etc/default/libc-locales
  ln -sf "/usr/share/zoneinfo/${timezone}" /etc/localtime
  xbps-reconfigure -f glibc-locales

  echo "${hostname}" > /etc/hostname
  echo '127.0.0.1 localhost' > /etc/hosts
  echo "127.0.1.1 ${hostname}" >> /etc/hosts
  echo '' >> /etc/hosts
  echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
  echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
  echo 'fe00::0 ip6-localnet' >> /etc/hosts
  echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
  echo 'ff02::1 ip6-allnodes' >> /etc/hosts
  echo 'ff02::2 ip6-allrouters' >> /etc/hosts
  echo 'ff02::3 ip6-allhosts' >> /etc/hosts

  echo 'VISUAL=nvim' >> /etc/environment
  echo 'EDITOR=nvim' >> /etc/environment

  sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

  for service in bluetoothd chronyd dbus NetworkManager nftables ngetty sddm snooze-weekly socklog-unix nanoklogd; do
    ln -s "/etc/sv/${service}" /etc/runit/runsvdir/default/
  done
  touch /etc/sv/agetty-tty{1..6}/down

  mkdir -p /etc/pipewire/pipewire.conf.d
  ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
  ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/

  mkdir -p /etc/alsa/conf.d
  ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
  ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d

  echo '/usr/lib/pipewire-0.3/jack' > /etc/ld.so.conf.d/pipewire-jack.conf
  ldconfig

  ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/

  mkdir -p /etc/NetworkManager/conf.d
  echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
  echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf
  echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
  echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

  echo '#!/usr/bin/nft -f' > /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo 'flush ruleset' >> /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo 'table inet filter {' >> /etc/nftables.conf
  echo '  chain input {' >> /etc/nftables.conf
  echo '    type filter hook input priority 0;' >> /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo '    # accept any localhost traffic' >> /etc/nftables.conf
  echo '    iif lo accept' >> /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo '    # accept traffic originated from us' >> /etc/nftables.conf
  echo '    ct state established,related accept' >> /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo '    # activate the following line to accept common local services' >> /etc/nftables.conf
  echo '    #tcp dport { 22, 80, 443 } ct state new accept' >> /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo '    # ICMPv6 packets which must not be dropped, see https://tools.ietf.org/html/rfc4890#section-4.4.1' >> /etc/nftables.conf
  echo '    meta nfproto ipv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, echo-reply, echo-request, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, 148, 149 } accept' >> /etc/nftables.conf
  echo '    ip6 saddr fe80::/10 icmpv6 type { 130, 131, 132, 143, 151, 152, 153 } accept' >> /etc/nftables.conf
  echo '' >> /etc/nftables.conf
  echo '    # count and drop any other traffic' >> /etc/nftables.conf
  echo '    counter drop' >> /etc/nftables.conf
  echo '  }' >> /etc/nftables.conf
  echo '}' >> /etc/nftables.conf

  echo '* hard core 0' >> /etc/security/limits.conf
  echo '* hard nofile 1048576' >> /etc/security/limits.conf

  mkdir -p /etc/sysctl.d
  echo 'net.core.default_qdisc = fq_codel' > /etc/sysctl.d/tcp-congestion.conf
  echo 'vm.max_map_count = 1048576' > /etc/sysctl.d/vm-maxmapcount.conf
  echo 'kernel.split_lock_mitigate = 0' > /etc/sysctl.d/kernel-splitlockmitigate.conf

  mkdir -p /etc/sddm.conf.d
  echo '[General]' > /etc/sddm.conf.d/sddm.conf
  echo 'Numlock=on' >> /etc/sddm.conf.d/sddm.conf
  echo '' >> /etc/sddm.conf.d/sddm.conf
  echo '[Theme]' >> /etc/sddm.conf.d/sddm.conf
  echo 'Current=breeze' >> /etc/sddm.conf.d/sddm.conf

  echo '[Icon Theme]' > /usr/share/icons/default/index.theme
  echo 'Inherits=breeze_cursors' >> /usr/share/icons/default/index.theme

  mkswap -U clear --size "${swapsize}" --file /swapfile > /dev/null
  echo '/swapfile none swap defaults 0 0' >> /etc/fstab

  bootctl install 2> /dev/null
  local partuuidroot
  partuuidroot=$(blkid -o value -s PARTUUID "/dev/${drive}p2")
  echo "CMDLINE='root=PARTUUID=${partuuidroot} rw ${boot_options}'" > /etc/default/systemd-boot

  mkdir -p /etc/cron.weekly
  echo '#!/bin/sh' > /etc/cron.weekly/fstrim
  echo 'LOG=/var/log/trim.log' >> /etc/cron.weekly/fstrim
  echo 'echo "*** $(date -R) ***" >> $LOG' >> /etc/cron.weekly/fstrim
  echo 'fstrim -av >> $LOG' >> /etc/cron.weekly/fstrim
  chmod +x /etc/cron.weekly/fstrim

  chsh -s /bin/bash > /dev/null
  useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input,network,bluetooth,socklog,gamemode "${user}"
  sudo -u "${user}" xdg-user-dirs-update
  local userpath
  userpath=$(eval echo ~"${user}")

  sudo -u "${user}" mkdir -p "${userpath}/git"
  cd "${userpath}/git"
  sudo -u "${user}" git clone --depth 1 https://gitlab.com/armond/Configurations.git 2> /dev/null
  sudo -u "${user}" cp -r "${userpath}/git/${config_directory}/." "${userpath}/"
  cp -r "${userpath}/git/${config_directory}/." ~root/
  sudo -u "${user}" curl -LO "https://github.com/microsoft/cascadia-code/releases/download/v${font}/CascadiaCode-${font}.zip" 2> /dev/null
  sudo -u "${user}" unzip "CascadiaCode-${font}" > /dev/null
  sudo -u "${user}" mkdir -p "${userpath}/.local/share/fonts/CascadiaCode"
  sudo -u "${user}" mv "${userpath}/git/ttf/*.ttf" "${userpath}/.local/share/fonts/CascadiaCode/"

  echo 'force_drivers+=" amdgpu nvidia nvidia_modeset nvidia_uvm nvidia_drm "' > /etc/dracut.conf.d/myflags.conf
  echo 'compress="lz4"' >> /etc/dracut.conf.d/myflags.conf
  echo 'hostonly="yes"' >> /etc/dracut.conf.d/myflags.conf
  xbps-reconfigure -f "${kernel}"

  printf "%s\n%s\n" "${rootpassword}" "${rootpassword}" | passwd > /dev/null 2>&1
  printf "%s\n%s\n" "${userpassword}" "${userpassword}" | passwd "${user}" > /dev/null 2>&1
}

# Main script logic
if [[ $# -eq 0 ]]; then
  echo 'Enter user name'
  read -r user
  echo "Enter ${user}'s password"
  read -rs userpassword
  echo 'Enter root password'
  read -rs rootpassword

  efibootmgr -bB {0..10} > /dev/null 2>&1
  rm -f /sys/firmware/efi/efivars/dump-*

  setup_partitions
  install_base_system
  configure_fstab
  chroot_setup "${user}" "${userpassword}" "${rootpassword}"
else
  configure_system "$@"
fi
