#!/bin/bash

drive='nvme0n1'
timezone='Europe/Lisbon'
mirror='https://repo-de.voidlinux.org'
kernel='linux6.12'
font='2407.24'
localization='en_US.UTF-8'
hostname='laptop'
swapsize='4G'
bootoptions='quiet loglevel=3 console=tty2 nmi_watchdog=0 nvidia_drm.modeset=1 nvidia_drm.fbdev=1'
configdirectory='Configurations/home'
install='
	base-devel bash-completion chrony efibootmgr fuse fwupd linux-tools lz4 ngetty parted psmisc snooze socklog-void strace systemd-boot thermald wget xdg-user-dirs xtools
	xorg-minimal xorg-apps nftables wireguard-tools
	pipewire alsa-pipewire gstreamer1-pipewire libjack-pipewire libspa-bluetooth libspa-jack libspa-v4l2 rtkit
	elogind nemo nemo-fileroller ffmpegthumbnailer gnome-terminal polkit-gnome blueman network-manager-applet
	qtile python3-psutil dmenu dunst pavucontrol picom rxvt-unicode
	bandwhich xdg-desktop-portal-gtk htop lutris neovim xsel nvtop weechat
	chromium firefox qutebrowser python3-adblock mpv yt-dlp qbittorrent telegram-desktop
	gnome-themes-extra nerd-fonts-symbols-ttf noto-fonts-cjk noto-fonts-emoji noto-fonts-ttf noto-fonts-ttf-extra
'
nonfree='
	libgcc-32bit libstdc++-32bit libdrm-32bit libglvnd-32bit vulkan-loader-32bit
	intel-ucode nvidia nvidia-libs-32bit
	pipewire-32bit alsa-pipewire-32bit gstreamer1-pipewire-32bit libjack-pipewire-32bit libspa-bluetooth-32bit libspa-jack-32bit libspa-v4l2-32bit
	steam gamemode libgamemode-32bit
	wine wine-32bit wine-gecko wine-mono winetricks
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done

	rm -f /sys/firmware/efi/efivars/dump-*

	wipefs -fa /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 1025MiB mkpart primary ext4 1025MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.ext4 -F /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mount --mkdir /dev/${drive}p1 /mnt/boot

	curl -LO ${mirror}/static/xbps-static-latest.x86_64-musl.tar.xz 2> /dev/null
	tar xf xbps-static-latest.x86_64-musl.tar.xz
	mkdir -p /mnt/var/db/xbps/keys
	cp ./var/db/xbps/keys/* /mnt/var/db/xbps/keys/
	XBPS_ARCH=x86_64 ./usr/bin/xbps-install.static -Sy -R ${mirror}/current -r /mnt base-system ${kernel} ${kernel}-headers

	partuuid1=$(blkid -o value -s PARTUUID /dev/${drive}p1)
	partuuid2=$(blkid -o value -s PARTUUID /dev/${drive}p2)
	echo "PARTUUID=${partuuid1} /boot vfat defaults 0 2" >> /mnt/etc/fstab
	echo "PARTUUID=${partuuid2} /     ext4 defaults 0 1" >> /mnt/etc/fstab

	for dir in dev proc sys run; do mkdir -p /mnt/$dir ; mount --rbind /$dir /mnt/$dir ; mount --make-rslave /mnt/$dir ; done
	cp --dereference /etc/resolv.conf /mnt/etc/
	cp $0 /mnt/setup
	chroot /mnt /bin/bash ./setup ${user} ${userpassword} ${rootpassword}
	rm -f /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	xbps-pkgdb -m manual linux-base

	mkdir -p /etc/xbps.d
	echo 'ignorepkg=linux' > /etc/xbps.d/99-ignore.conf
	echo 'ignorepkg=linux-headers' >> /etc/xbps.d/99-ignore.conf
	xbps-remove -Ry linux

	cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
	sed -i "s|https://repo-default.voidlinux.org|${mirror}|g" /etc/xbps.d/*-repository-*.conf

	xbps-install -Sy
	xbps-install -y ${install}
	xbps-install -y void-repo-nonfree void-repo-multilib{,-nonfree}

	cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
	sed -i "s|https://repo-default.voidlinux.org|${mirror}|g" /etc/xbps.d/*-repository-*.conf

	xbps-install -Sy
	xbps-install -y ${nonfree}

	echo "LANG=${localization}" > /etc/locale.conf
	sed -i "/${localization}/s/^#//g" /etc/default/libc-locales
	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	xbps-reconfigure -f glibc-locales

	echo "${hostname}" > /etc/hostname

	echo '127.0.0.1 localhost' > /etc/hosts
	echo "127.0.1.1 ${hostname}" >> /etc/hosts
	echo '' >> /etc/hosts
	echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
	echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
	echo 'fe00::0 ip6-localnet' >> /etc/hosts
	echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
	echo 'ff02::1 ip6-allnodes' >> /etc/hosts
	echo 'ff02::2 ip6-allrouters' >> /etc/hosts
	echo 'ff02::3 ip6-allhosts' >> /etc/hosts

	echo 'VISUAL=nvim' >> /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment
	echo 'GTK_THEME=Adwaita:dark' >> /etc/environment

	sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	ln -s /etc/sv/bluetoothd /etc/runit/runsvdir/default/
	ln -s /etc/sv/chronyd /etc/runit/runsvdir/default/
	ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
	ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/
	ln -s /etc/sv/nftables /etc/runit/runsvdir/default/
	ln -s /etc/sv/ngetty /etc/runit/runsvdir/default/
	ln -s /etc/sv/snooze-weekly /etc/runit/runsvdir/default/
	ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/
	ln -s /etc/sv/nanoklogd /etc/runit/runsvdir/default/
	ln -s /etc/sv/thermald /etc/runit/runsvdir/default/
	touch /etc/sv/agetty-tty{1..6}/down

	mkdir -p /etc/pipewire/pipewire.conf.d
	ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
	ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/

	mkdir -p /etc/alsa/conf.d
	ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
	ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d

	echo '/usr/lib/pipewire-0.3/jack' > /etc/ld.so.conf.d/pipewire-jack.conf
	ldconfig

	mkdir -p /etc/NetworkManager/conf.d
	echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
	echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf
	echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
	echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

	echo '#!/usr/bin/nft -f' > /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'flush ruleset' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'table inet filter {' >> /etc/nftables.conf
	echo '	chain input {' >> /etc/nftables.conf
	echo '		type filter hook input priority 0;' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# accept any localhost traffic' >> /etc/nftables.conf
	echo '		iif lo accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# accept traffic originated from us' >> /etc/nftables.conf
	echo '		ct state established,related accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# activate the following line to accept common local services' >> /etc/nftables.conf
	echo '		#tcp dport { 22, 80, 443 } ct state new accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# ICMPv6 packets which must not be dropped, see https://tools.ietf.org/html/rfc4890#section-4.4.1' >> /etc/nftables.conf
	echo '		meta nfproto ipv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, echo-reply, echo-request, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, 148, 149 } accept' >> /etc/nftables.conf
	echo '		ip6 saddr fe80::/10 icmpv6 type { 130, 131, 132, 143, 151, 152, 153 } accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# count and drop any other traffic' >> /etc/nftables.conf
	echo '		counter drop' >> /etc/nftables.conf
	echo '	}' >> /etc/nftables.conf
	echo '}' >> /etc/nftables.conf

	echo '* hard core 0' >> /etc/security/limits.conf
	echo '* hard nofile 1048576' >> /etc/security/limits.conf

	mkdir -p /etc/sysctl.d
	echo 'net.core.default_qdisc = fq_codel' > /etc/sysctl.d/tcp-congestion.conf
	echo 'vm.max_map_count = 1048576' > /etc/sysctl.d/vm-maxmapcount.conf
	echo 'kernel.split_lock_mitigate = 0' > /etc/sysctl.d/kernel-splitlockmitigate.conf

	mkdir -p /etc/elogind/logind.conf.d
	echo '[Login]' > /etc/elogind/logind.conf.d/logind.conf
	echo 'HandleLidSwitch=ignore' >> /etc/elogind/logind.conf.d/logind.conf

	mkswap -U clear --size ${swapsize} --file /swapfile > /dev/null
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab

	bootctl install 2> /dev/null
	partuuidroot=$(blkid -o value -s PARTUUID /dev/${drive}p2)
	echo "CMDLINE='root=PARTUUID=${partuuidroot} rw ${bootoptions}'" > /etc/default/systemd-boot

	mkdir -p /etc/cron.weekly
	echo '#!/bin/sh' > /etc/cron.weekly/fstrim
	echo 'LOG=/var/log/trim.log' >> /etc/cron.weekly/fstrim
	echo 'echo "*** $(date -R) ***" >> $LOG' >> /etc/cron.weekly/fstrim
	echo 'fstrim -av >> $LOG' >> /etc/cron.weekly/fstrim
	chmod +x /etc/cron.weekly/fstrim

	chsh -s /bin/bash > /dev/null
	useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input,network,bluetooth,socklog,gamemode ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git 2> /dev/null
	sudo -u ${user} cp -r ${userpath}/git/${configdirectory}/. ${userpath}/
	sudo -u ${user} chmod +x ${userpath}/.config/qtile/autostart.sh
	cp -r ${userpath}/git/${configdirectory}/. ~root/
	sudo -u ${user} curl -LO https://github.com/microsoft/cascadia-code/releases/download/v${font}/CascadiaCode-${font}.zip 2> /dev/null
	sudo -u ${user} unzip CascadiaCode-${font} > /dev/null
	sudo -u ${user} mkdir -p ${userpath}/.local/share/fonts/CascadiaCode
	sudo -u ${user} mv ${userpath}/git/ttf/*.ttf ${userpath}/.local/share/fonts/CascadiaCode/

	sudo -u ${user} dbus-run-session -- gsettings set org.gnome.desktop.interface color-scheme prefer-dark 2> /dev/null

	echo 'force_drivers+=" nvidia nvidia_modeset nvidia_uvm nvidia_drm "' > /etc/dracut.conf.d/myflags.conf
	echo 'compress="lz4"' >> /etc/dracut.conf.d/myflags.conf
	echo 'hostonly="yes"' >> /etc/dracut.conf.d/myflags.conf
	xbps-reconfigure -f ${kernel}

	printf "${rootpassword}\n${rootpassword}\n" | passwd > /dev/null 2> /dev/null
	printf "${userpassword}\n${userpassword}\n" | passwd ${user} > /dev/null 2> /dev/null
fi
