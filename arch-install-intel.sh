#!/usr/bin/bash

drive='nvme0n1'
timezone='Europe/Lisbon'
localization='en_US.UTF-8'
keyboardlayout='us'
hostname='laptop'
swapsize='2048'
bootoptions='quiet loglevel=3 systemd.show_status=auto rd.udev.log_level=3 intel_pstate=passive cpufreq.default_governor=ondemand nmi_watchdog=0 nvidia_drm.modeset=1 nvidia_drm.fbdev=1'
configdirectory='Configurations/home'
services='bluetooth fstrim.timer NetworkManager nftables openntpd thermald'
install='
	base base-devel linux-zen dkms linux-zen-headers linux-firmware intel-ucode thermald
	bash bash-completion curl git linux-tools lz4 openntpd pacman-contrib wget xdg-user-dirs dosfstools e2fsprogs efibootmgr parted fwupd udisks2 xfsprogs
	xorg-server xorg-apps xorg-xinit
	vulkan-icd-loader nvidia-dkms nvidia-utils nvidia-settings
	bluez bluez-utils networkmanager nftables openresolv wireguard-tools
	pipewire pipewire-audio pipewire-alsa pipewire-pulse pipewire-jack wireplumber realtime-privileges
	xfce4 xfce4-goodies file-roller gvfs blueman network-manager-applet i3-wm dmenu i3status dex pavucontrol
	gnome-themes-extra materia-gtk-theme elementary-icon-theme
	bandwhich flatpak xdg-desktop-portal xdg-desktop-portal-gtk htop man-db neovim xsel nvtop weechat
	chromium firefox qutebrowser python-adblock mpv yt-dlp qbittorrent telegram-desktop
	noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra ttf-cascadia-code ttf-nerd-fonts-symbols
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done

	rm -f /sys/firmware/efi/efivars/dump-*

	wipefs -fa /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 1025MiB mkpart primary xfs 1025MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.xfs -f /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mount --mkdir /dev/${drive}p1 /mnt/boot

	echo 'Server = https://ftp.rnl.tecnico.ulisboa.pt/pub/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

	sed -i -e '/Color/s/^#//g' -e '/ParallelDownloads/s/^#//g' /etc/pacman.conf

	pacstrap -K /mnt ${install}

	genfstab -U /mnt >> /mnt/etc/fstab

	cp $0 /mnt/setup
	arch-chroot /mnt ./setup ${user} ${userpassword} ${rootpassword}
	rm -f /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	hwclock --systohc

	sed -i "/${localization}/s/^#//g" /etc/locale.gen
	locale-gen > /dev/null
	echo "LANG=${localization}" > /etc/locale.conf
	echo "KEYMAP=${keyboardlayout}" > /etc/vconsole.conf

	echo "${hostname}" > /etc/hostname

	echo '127.0.0.1 localhost' > /etc/hosts
	echo "127.0.1.1 ${hostname}" >> /etc/hosts
	echo '' >> /etc/hosts
	echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
	echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
	echo 'fe00::0 ip6-localnet' >> /etc/hosts
	echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
	echo 'ff02::1 ip6-allnodes' >> /etc/hosts
	echo 'ff02::2 ip6-allrouters' >> /etc/hosts
	echo 'ff02::3 ip6-allhosts' >> /etc/hosts

	echo 'VISUAL=nvim' >> /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment

	sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	systemctl stop systemd-timesyncd 2> /dev/null
	systemctl disable systemd-timesyncd 2> /dev/null
	systemctl mask systemd-timesyncd 2> /dev/null
	systemctl enable ${services} 2> /dev/null

	echo 'Section "Device"' > /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Identifier     "Device0"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Driver         "nvidia"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    VendorName     "NVIDIA Corporation"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    BoardName      "NVIDIA GeForce RTX 4070 Laptop GPU"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo 'Section "Screen"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Identifier     "Screen0"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Device         "Device0"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Monitor        "Monitor0"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Option         "ForceFullCompositionPipeline" "on"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Option         "AllowIndirectGLXProtocol" "off"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo '    Option         "TripleBuffer" "on"' >> /etc/X11/xorg.conf.d/20-nvidia.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/20-nvidia.conf

	echo 'Section "InputClass"' > /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Identifier "touchpad"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Driver "libinput"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  MatchIsTouchpad "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Option "NaturalScrolling" "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Option "Tapping" "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/30-touchpad.conf

	echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
	echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf
	echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
	echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

	echo '#!/usr/bin/nft -f' > /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'flush ruleset' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'table inet filter {' >> /etc/nftables.conf
	echo '	chain input {' >> /etc/nftables.conf
	echo '		type filter hook input priority 0;' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# accept any localhost traffic' >> /etc/nftables.conf
	echo '		iif lo accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# accept traffic originated from us' >> /etc/nftables.conf
	echo '		ct state established,related accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# activate the following line to accept common local services' >> /etc/nftables.conf
	echo '		#tcp dport { 22, 80, 443 } ct state new accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# ICMPv6 packets which must not be dropped, see https://tools.ietf.org/html/rfc4890#section-4.4.1' >> /etc/nftables.conf
	echo '		meta nfproto ipv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, echo-reply, echo-request, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, 148, 149 } accept' >> /etc/nftables.conf
	echo '		ip6 saddr fe80::/10 icmpv6 type { 130, 131, 132, 143, 151, 152, 153 } accept' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '		# count and drop any other traffic' >> /etc/nftables.conf
	echo '		counter drop' >> /etc/nftables.conf
	echo '	}' >> /etc/nftables.conf
	echo '}' >> /etc/nftables.conf

	echo 'tcp_bbr' > /etc/modules-load.d/tcp-congestion.conf
	echo 'net.core.default_qdisc = cake' > /etc/sysctl.d/tcp-congestion.conf
	echo 'net.ipv4.tcp_congestion_control = bbr' >> /etc/sysctl.d/tcp-congestion.conf

	echo 'kernel.core_pattern=/dev/null' > /etc/sysctl.d/50-coredump.conf

	echo 'MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)' > /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'BINARIES=()' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'FILES=()' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'HOOKS=(base systemd autodetect microcode modconf block filesystems fsck)' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'COMPRESSION="lz4"' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'COMPRESSION_OPTIONS=(-9)' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'MODULES_DECOMPRESS="yes"' >> /etc/mkinitcpio.conf.d/myhooks.conf

	sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf

	sed -i -e '/RuntimeWatchdogSec/s/^#//g' -e 's/#RebootWatchdogSec=10min/RebootWatchdogSec=off/' -e '/KExecWatchdogSec/s/^#//g' /etc/systemd/system.conf

	sed -i 's/#DefaultLimitNOFILE=1024:524288/DefaultLimitNOFILE=1024:1048576/' /etc/systemd/system.conf
	sed -i 's/#DefaultLimitNOFILE=/DefaultLimitNOFILE=1024:1048576/' /etc/systemd/user.conf

	sed -i 's/#SystemMaxUse=/SystemMaxUse=50M/' /etc/systemd/journald.conf

	sed -i -e '/Color/s/^#//g' -e '/ParallelDownloads/s/^#//g' /etc/pacman.conf

	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 0600 /swapfile
	mkswap -U clear /swapfile > /dev/null
	echo '# /swapfile' >> /etc/fstab
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab

	bootctl install 2> /dev/null
	uuidroot=$(blkid -o value -s UUID /dev/${drive}p2)
	echo 'title   Arch Linux' > /boot/loader/entries/arch.conf
	echo 'linux   /vmlinuz-linux-zen' >> /boot/loader/entries/arch.conf
	echo 'initrd  /initramfs-linux-zen.img' >> /boot/loader/entries/arch.conf
	echo "options root=UUID=${uuidroot} rw ${bootoptions}" >> /boot/loader/entries/arch.conf

	useradd -m -G wheel,realtime -s /usr/bin/bash ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	sudo -u ${user} dbus-run-session -- gsettings set org.gnome.desktop.interface color-scheme prefer-dark 2> /dev/null

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git 2> /dev/null
	sudo -u ${user} cp -r ${userpath}/git/${configdirectory}/. ${userpath}/
	cp -r ${userpath}/git/${configdirectory}/. ~root/

	mkinitcpio -p linux-zen

	printf "${rootpassword}\n${rootpassword}\n" | passwd > /dev/null 2> /dev/null
	printf "${userpassword}\n${userpassword}\n" | passwd ${user} > /dev/null 2> /dev/null
fi
