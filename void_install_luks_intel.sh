#!/bin/sh

set -eu

readonly xbps_arch="x86_64"
readonly xbps_repo="https://repo-fastly.voidlinux.org"
readonly target_dir="/mnt"
readonly drive="/dev/nvme0n1"
readonly esp_partition="${drive}p1"
readonly root_partition="${drive}p2"
readonly cryptname="cryptroot"
readonly esp_mount="${target_dir}/boot"
readonly swap_size="4G"
readonly timezone="Africa/Luanda"
readonly localization="en_US.UTF-8"
readonly keyboard_layout="us"
readonly hostname="laptop"
readonly kernel="linux6.13"
readonly font="2407.24"
readonly boot_options="quiet loglevel=3 nmi_watchdog=0 nvidia_drm.modeset=1"
readonly config_directory="Configurations/home"
readonly install_pkgs="
  base-devel chrony efibootmgr fuse fwupd linux-tools mkinitcpio-encrypt parted psmisc
  snooze socklog-void strace systemd-boot thermald wget xdg-user-dirs xtools zsh-completions
  xorg-minimal xorg-apps
  nftables wireguard-tools pipewire alsa-pipewire gstreamer1-pipewire libjack-pipewire
  libspa-bluetooth libspa-jack libspa-v4l2 rtkit kde-plasma kde-baseapps ark gwenview
  kcalc kdiff3 konversation krusader ksystemlog okular spectacle bluez NetworkManager
  bandwhich ghostty htop lutris neovim wl-clipboard xsel nvtop weechat
  chromium qutebrowser python3-adblock mpv yt-dlp qbittorrent telegram-desktop
  gnome-themes-extra noto-fonts-cjk noto-fonts-emoji noto-fonts-ttf noto-fonts-ttf-extra
"
readonly nonfree_pkgs="
  libgcc-32bit libstdc++-32bit libdrm-32bit libglvnd-32bit vulkan-loader-32bit
  intel-ucode nvidia nvidia-libs-32bit
  pipewire-32bit alsa-pipewire-32bit gstreamer1-pipewire-32bit libjack-pipewire-32bit
  libspa-bluetooth-32bit libspa-jack-32bit libspa-v4l2-32bit steam gamemode
  libgamemode-32bit wine wine-32bit wine-gecko wine-mono winetricks
"

username=""
user_password=""
root_password=""
luks_password=""
tmp_xbps_dir=""

cleanup() {
  exit_code=$?
  unset user_password root_password luks_password 2>/dev/null || true

  mountpoint -q "${esp_mount}" && umount -Rf "${esp_mount}" 2>/dev/null || true
  mountpoint -q "${target_dir}" && umount -Rf "${target_dir}" 2>/dev/null || true

  cryptsetup status "${cryptname}" >/dev/null 2>&1 && cryptsetup close "${cryptname}" 2>/dev/null || true

  [ -n "${tmp_xbps_dir}" ] && rm -rf "${tmp_xbps_dir}"

  exit ${exit_code}
}

trap 'cleanup' EXIT INT TERM HUP

check_interactive() {
  if [ ! -t 0 ] || [ ! -t 1 ]; then
    echo "ERROR: Run interactively (e.g., ./script.sh, not via cron/pipe)." > /dev/tty
    exit 1
  fi
}

check_root() {
  if [ "$(id -u)" -ne 0 ]; then
    cat <<EOF > /dev/tty
ERROR: This script requires root privileges.
       Re-run with either:
       1. Direct root login: sudo -i
       2. Execute with sudo: sudo $0
EOF
    exit 1
  fi
}

check_efi() {
  if [ ! -d /sys/firmware/efi ]; then
    echo "ERROR: Not booted in UEFI mode!" >&2
    exit 1
  fi
}

check_cmd() {
  for cmd in cryptsetup curl tar xz; do
    if ! command -v "${cmd}" >/dev/null 2>&1; then
      echo "Required command missing: ${cmd}" >&2
      exit 1
    fi
  done
}

get_credentials() {
  printf "Enter username: "
  read -r username </dev/tty
  printf "Enter password for %s: " "${username}"
  read -r user_password </dev/tty
  printf "Enter root password: "
  read -r root_password </dev/tty
  printf "Enter LUKS encryption password: "
  read -r luks_password </dev/tty
  printf "\n"
}

validate_variables() {
  missing=""
  [ -z "${username}" ] && missing="${missing} username"
  [ -z "${user_password}" ] && missing="${missing} user_password"
  [ -z "${root_password}" ] && missing="${missing} root_password"
  [ -z "${luks_password}" ] && missing="${missing} luks_password"

  if [ -n "${missing}" ]; then
    echo "ERROR: Missing required values for:${missing}" >&2
    exit 1
  fi
}

clear_efi() {
  echo "Clearing UEFI entries..." >&2
  rm -f /sys/firmware/efi/efivars/Boot[0-9A-F][0-9A-F][0-9A-F][0-9A-F]-8be4df61-93ca-11d2-aa0d-00e098032b8c 2>/dev/null
  rm -f /sys/firmware/efi/efivars/dump-* 2>/dev/null
}

partition_disk() {
  echo "Partitioning ${drive} with 1024MiB ESP..." >&2
  sfdisk "${drive}" <<EOF
label: gpt
start=1MiB, size=1024MiB, type=uefi
start=1025MiB, type=linux
EOF
  udevadm settle
  blockdev --rereadpt "${drive}"
}

format_partitions() {
  echo "Formatting partitions..."
  mkfs.fat -F32 "${esp_partition}"
  printf "%s" "${luks_password}" | cryptsetup luksFormat --type luks2 --batch-mode \
    --pbkdf argon2id --hash blake2b-512 --key-file=- "${root_partition}"
  printf "%s" "${luks_password}" | cryptsetup open --allow-discards --persistent \
    --key-file=- "${root_partition}" "${cryptname}"
  mkfs.ext4 -F "/dev/mapper/${cryptname}"
}

mount_filesystems() {
  echo "Mounting filesystems..."
  mount "/dev/mapper/${cryptname}" "${target_dir}"
  mkdir -p "${esp_mount}"
  mount "${esp_partition}" "${esp_mount}"
}

get_latest_xbps() {
  _glb_static_url="${xbps_repo}/static"
  _glb_curl_output="" _glb_result="" xbps_version="" xbps_url=""

  if ! _glb_curl_output=$(curl -fsL "${_glb_static_url}/sha256sums.txt"); then
    echo "ERROR: Repository index download failed" >&2
    return 1
  fi

  _glb_result=$(echo "${_glb_curl_output}" | \
    grep -E "xbps-static-static-[0-9]+\.[0-9]+(\.[0-9]+)?_[0-9]+\.${xbps_arch}-musl\.tar\.xz" | \
    LC_ALL=C awk -v arch="${xbps_arch}" '
    BEGIN {
      max_val = 0
      prefix = "xbps-static-static-"
      suffix = "." arch "-musl.tar.xz"
    }
    {
      hash = substr($0, 1, 64)
      filename = substr($0, 67)
      gsub(/^[ \t*]+/, "", filename)

      if (filename !~ /^xbps-static-static-[0-9]+\.[0-9]+(\.[0-9]+)?_[0-9]+\.[a-z0-9_-]+\.tar\.xz$/) next

      ver_start = length(prefix) + 1
      ver_end = length(filename) - length(suffix)
      version_str = substr(filename, ver_start, ver_end - ver_start + 1)

      split(version_str, parts, "[._]")
      major = parts[1] + 0
      minor = parts[2] + 0
      patch = (length(parts) >= 4) ? parts[3] + 0 : 0
      revision = parts[length(parts)] + 0

      current_val = major * 1000000000 + minor * 1000000 + patch * 1000 + revision
      if (current_val > max_val) {
        max_val = current_val
        best_hash = hash
        best_version = version_str
      }
    }
    END {
      if (best_version != "") print best_hash " " best_version
      else exit 1
    }') || {
    echo "ERROR: No valid packages for ${xbps_arch}" >&2
    return 1
  }

  xbps_hash="${_glb_result%% *}"
  xbps_version="${_glb_result#* }"
  xbps_url="${_glb_static_url}/xbps-static-static-${xbps_version}.${xbps_arch}-musl.tar.xz"

  : "${xbps_version:?ERROR: Version not set}" \
    "${xbps_url:?ERROR: URL generation failed}" \
    "${xbps_hash:?ERROR: Hash not found}"

  echo "Resolved package: ${xbps_version}"
  return 0
}

download_xbps() {
  tmp_xbps_dir=$(mktemp -d) || {
    echo "ERROR: Failed to create temporary directory" >&2
    return 1
  }

  echo "Downloading xbps-static (${xbps_version})..."
  if ! curl -fsL "${xbps_url}" -o "${tmp_xbps_dir}/xbps-static.tar.xz"; then
    echo "ERROR: Failed to download ${xbps_url}" >&2
    return 1
  fi

  echo "Verifying checksum..."
  if ! echo "${xbps_hash}  ${tmp_xbps_dir}/xbps-static.tar.xz" | sha256sum -c - >/dev/null; then
    echo "ERROR: Checksum verification failed" >&2
    return 1
  fi

  echo "Extracting xbps-static..."
  if ! tar -xJf "${tmp_xbps_dir}/xbps-static.tar.xz" -C "${tmp_xbps_dir}"; then
    echo "ERROR: Failed to extract package" >&2
    return 1
  fi

  if [ ! -d "${tmp_xbps_dir}/var/db/xbps/keys" ]; then
    echo "ERROR: Downloaded package missing keys directory" >&2
    return 1
  fi

  export PATH="${tmp_xbps_dir}/usr/bin:${PATH}"
  return 0
}

install_base() {
  mkdir -p "${target_dir}/var/db/xbps/keys" || {
    echo "ERROR: Failed to create keys directory" >&2
    return 1
  }

  keys_src="${tmp_xbps_dir}/var/db/xbps/keys"
  if [ ! -d "${keys_src}" ]; then
    echo "ERROR: Source keys directory missing: ${keys_src}" >&2
    return 1
  fi

  if [ -z "$(ls -A "${keys_src}" 2>/dev/null)" ]; then
    echo "ERROR: No keys found in ${keys_src}" >&2
    return 1
  fi

  echo "Copying XBPS keys..."
  for keyfile in "${keys_src}"/*; do
    if [ -f "${keyfile}" ]; then
      cp -p "${keyfile}" "${target_dir}/var/db/xbps/keys/" || {
        echo "ERROR: Failed to copy ${keyfile}" >&2
        return 1
      }
    fi
  done

  echo "Installing base system..."
  XBPS_ARCH="${xbps_arch}" xbps-install -Sy -r "${target_dir}" \
    -R "${xbps_repo}/current" base-system "${kernel}" "${kernel}-headers" || {
    echo "ERROR: Base system installation failed" >&2
    return 1
  }

  return 0
}

configure_chroot() {
  trap - EXIT INT TERM HUP

  . /tmp/install_vars.sh

  xbps-pkgdb -m manual linux-base "${kernel}" "${kernel}-headers"

  mkdir -p /etc/xbps.d
  {
    echo 'ignorepkg=linux'
    echo 'ignorepkg=linux-headers'
  } > /etc/xbps.d/99-ignore.conf
  xbps-remove -Ry linux

  cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
  sed -i "s|https://repo-default.voidlinux.org|${xbps_repo}|g" /etc/xbps.d/*-repository-*.conf

  xbps-install -Sy
  xbps-install -uy xbps
  xbps-install -y ${install_pkgs}
  xbps-install -y void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree

  cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
  sed -i "s|https://repo-default.voidlinux.org|${xbps_repo}|g" /etc/xbps.d/*-repository-*.conf

  xbps-install -Sy
  xbps-install -y ${nonfree_pkgs}

  sed -i "/^#${localization}/s/#//" /etc/default/libc-locales
  echo "LANG=${localization}" > /etc/locale.conf
  ln -sf "/usr/share/zoneinfo/${timezone}" /etc/localtime
  sed -i -e "s/^#KEYMAP=\"es\"/KEYMAP=\"${keyboard_layout}\"/" -e 's/^#FONT="lat9w-16"/FONT="Lat2-Terminus16"/' /etc/rc.conf
  xbps-reconfigure -f glibc-locales

  echo "${hostname}" > /etc/hostname

  {
    echo '127.0.0.1 localhost'
    echo "127.0.1.1 ${hostname}"
    echo
    echo '# The following lines are desirable for IPv6 capable hosts'
    echo '::1     ip6-localhost ip6-loopback'
    echo 'fe00::0 ip6-localnet'
    echo 'ff00::0 ip6-mcastprefix'
    echo 'ff02::1 ip6-allnodes'
    echo 'ff02::2 ip6-allrouters'
    echo 'ff02::3 ip6-allhosts'
  } > /etc/hosts

  {
    echo 'VISUAL=nvim'
    echo 'EDITOR=nvim'
    echo 'POWERDEVIL_NO_DDCUTIL=1'
  } >> /etc/environment

  mkdir -p /etc/sudoers.d
  echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel

  printf "%s\n" bluetoothd chronyd dbus NetworkManager nftables sddm snooze-weekly socklog-unix nanoklogd thermald |
  sed 's@.*@/etc/sv/& /etc/runit/runsvdir/default/&@' |
  xargs -n 2 ln -sf

  mkdir -p /etc/pipewire/pipewire.conf.d
  ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
  ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/

  mkdir -p /etc/alsa/conf.d
  ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
  ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d

  mkdir -p /etc/ld.so.conf.d
  echo '/usr/lib/pipewire-0.3/jack' > /etc/ld.so.conf.d/pipewire-jack.conf
  ldconfig

  ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/

  mkdir -p /etc/NetworkManager/conf.d
  {
    echo '[keyfile]'
    echo 'unmanaged-devices=type:wireguard'
  } > /etc/NetworkManager/conf.d/unmanaged.conf

  {
    echo '[main]'
    echo 'rc-manager=resolvconf'
  } > /etc/NetworkManager/conf.d/rc-manager.conf

  {
    echo '#!/usr/bin/nft -f'
    echo
    echo 'flush ruleset'
    echo
    echo 'table inet filter {'
    echo '  chain input {'
    echo '    type filter hook input priority 0;'
    echo
    echo '    # accept any localhost traffic'
    echo '    iif lo accept'
    echo
    echo '    # accept traffic originated from us'
    echo '    ct state established,related accept'
    echo
    echo '    # activate to accept common local services'
    echo '    #tcp dport { 22, 80, 443 } ct state new accept'
    echo
    echo '    # ICMPv6 exceptions (RFC4890)'
    echo '    meta nfproto ipv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, echo-reply, echo-request, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, 148, 149 } accept'
    echo '    ip6 saddr fe80::/10 icmpv6 type { 130, 131, 132, 143, 151, 152, 153 } accept'
    echo
    echo '    # count and drop any other traffic'
    echo '    counter drop'
    echo '  }'
    echo '}'
  } > /etc/nftables.conf

  {
    echo '* hard core 0'
    echo '* hard nofile 1048576'
  } >> /etc/security/limits.conf

  mkdir -p /etc/sysctl.d
  echo 'net.core.default_qdisc = fq_codel' > /etc/sysctl.d/tcp-congestion.conf
  echo 'vm.max_map_count = 1048576' > /etc/sysctl.d/vm-maxmapcount.conf
  echo 'kernel.split_lock_mitigate = 0' > /etc/sysctl.d/kernel-splitlockmitigate.conf

  mkdir -p /etc/sddm.conf.d
  {
    echo '[General]'
    echo 'Numlock=on'
    echo
    echo '[Theme]'
    echo 'Current=breeze'
  } > /etc/sddm.conf.d/sddm.conf

  {
    echo '[General]'
    echo 'DisplayServer=wayland'
    echo 'GreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell'
    echo
    echo '[Wayland]'
    echo 'CompositorCommand=kwin_wayland --drm --no-lockscreen --no-global-shortcuts --locale1'
  } > /etc/sddm.conf.d/10-wayland.conf

  mkdir -p /var/lib/sddm/.config
  {
    echo '[Keyboard]'
    echo 'NumLock=0'
  } > /var/lib/sddm/.config/kcminputrc

  mkdir -p /usr/share/icons/default
  {
    echo '[Icon Theme]'
    echo 'Inherits=breeze_cursors'
  } > /usr/share/icons/default/index.theme

  swapon --show=NAME | grep -q '/swapfile' && swapoff /swapfile
  rm -f /swapfile
  mkswap -U clear --size "${swap_size}" --file /swapfile
  chmod 600 /swapfile
  swapon /swapfile
  xgenfstab -U / > /etc/fstab
  swapoff /swapfile

  bootctl install
  echo "CMDLINE=\"cryptdevice=UUID=${luks_uuid}:${cryptname} root=UUID=${root_uuid} ro ${boot_options}\"" > /etc/default/systemd-boot

  mkdir -p /etc/cron.weekly
  {
    echo '#!/bin/sh'
    echo 'set -eu'
    echo 'readonly log="/var/log/trim.log"'
    echo 'printf "*** %s ***\n" "$(date -R)" >> "${log}"'
    echo 'fstrim -av >> "${log}" 2>&1'
  } > /etc/cron.weekly/fstrim
  chmod +x /etc/cron.weekly/fstrim

  chsh -s /bin/zsh
  useradd -m -s /bin/zsh -U -G wheel,users,audio,video,cdrom,input,network,bluetooth,socklog,gamemode "${username}"
  sudo -u "${username}" xdg-user-dirs-update
  userpath=$(getent passwd "${username}" | cut -d: -f6)

  sudo -u "${username}" mkdir -p "${userpath}/git"
  cd "${userpath}/git"
  sudo -u "${username}" git clone --depth 1 https://gitlab.com/armond/Configurations.git
  sudo -u "${username}" cp -r "${userpath}/git/${config_directory}/." "${userpath}/"
  cp -r "${userpath}/git/${config_directory}/." /root/
  sudo -u "${username}" curl -LO "https://github.com/microsoft/cascadia-code/releases/download/v${font}/CascadiaCode-${font}.zip"
  sudo -u "${username}" unzip "CascadiaCode-${font}.zip"
  sudo -u "${username}" mkdir -p "${userpath}/.local/share/fonts/CascadiaCode"
  sudo -u "${username}" mv "${userpath}/git/ttf/"*.ttf "${userpath}/.local/share/fonts/CascadiaCode/"

  mkdir -p /etc/mkinitcpio.conf.d
  {
    echo 'MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)'
    echo 'BINARIES=()'
    echo 'FILES=()'
    echo 'HOOKS=(base udev autodetect microcode modconf keyboard keymap consolefont block encrypt filesystems fsck)'
  } > /etc/mkinitcpio.conf.d/myhooks.conf

  xbps-alternatives -s mkinitcpio
  xbps-reconfigure -f "${kernel}"

  printf "%s\n%s\n" "${user_password}" "${user_password}" | passwd "${username}" >/dev/null 2>&1
  printf "%s\n%s\n" "${root_password}" "${root_password}" | passwd root >/dev/null 2>&1

  rm -f /tmp/install_vars.sh
  rm -f "$0"

  exit 0
}

configure_system() {
  luks_uuid=$(blkid -s UUID -o value "${root_partition}")
  root_uuid=$(blkid -s UUID -o value "/dev/mapper/${cryptname}")

  for dir in dev proc sys; do
    mount --rbind "/${dir}" "${target_dir}/${dir}"
    mount --make-rslave "${target_dir}/${dir}"
  done
  cp /etc/resolv.conf "${target_dir}/etc/"

  mkdir -p "${target_dir}/tmp"
  {
    printf "luks_uuid='%s'\n" "${luks_uuid}"
    printf "root_uuid='%s'\n" "${root_uuid}"
    printf "username='%s'\n" "${username}"
    printf "user_password='%s'\n" "${user_password}"
    printf "root_password='%s'\n" "${root_password}"
  } > "${target_dir}/tmp/install_vars.sh"

  cp "$0" "${target_dir}/tmp/install.sh"
  chmod +x "${target_dir}/tmp/install.sh"

  chroot "${target_dir}" /bin/sh "/tmp/install.sh" --chroot
}

main() {
  case "${1:-}" in
    --chroot)
      configure_chroot
      ;;
    *)
      check_interactive
      check_root
      check_efi
      check_cmd
      get_credentials
      validate_variables
      clear_efi
      partition_disk
      format_partitions
      mount_filesystems
      get_latest_xbps
      download_xbps
      install_base
      configure_system
      ;;
  esac
}

main "$@"
