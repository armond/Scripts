#!/bin/bash

setenforce 0

drive='nvme0n1'
timezone='Europe/Lisbon'
localization='en_US.UTF-8'
keyboardlayout='us'
hostname='pc'
swapsize='2048'
configdirectory='Configurations/home'
install='
	@hardware-support @base-x @development-tools @gnome-desktop firefox curl git neovim shim-x64 efibootmgr nftables kitty
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	wipefs --force --all /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 551MiB mkpart primary ext4 551MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.ext4 -F /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mkdir -p /mnt/boot/efi
	mount /dev/${drive}p1 /mnt/boot/efi

	dnf --releasever=36 --installroot=/mnt --assumeyes groupinstall minimal-environment

	systemd-firstboot --root=/mnt --locale=${localization} --keymap=${keyboardlayout} --timezone=${timezone} --hostname=${hostname}--setup-machine-id

	mount --types proc /proc /mnt/proc
	mount --rbind /sys /mnt/sys
	mount --make-rslave /mnt/sys
	mount --rbind /dev /mnt/dev
	mount --make-rslave /mnt/dev
	mount --bind /run /mnt/run
	mount --make-slave /mnt/run
	cp --dereference /etc/resolv.conf /mnt/etc/
	cp $0 /mnt/setup
	chroot /mnt /bin/bash ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	dnf --assumeyes install ${install}

	echo 'VISUAL=nvim' >> /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment

	sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	systemctl set-default graphical.target 2> /dev/null

	echo '#!/usr/bin/nft -f' > /etc/nftables.conf
	echo '# vim:set ts=2 sw=2 et:' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '# IPv4/IPv6 Simple & Safe firewall ruleset.' >> /etc/nftables.conf
	echo '# More examples in /usr/share/nftables/ and /usr/share/doc/nftables/examples/.' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'table inet filter' >> /etc/nftables.conf
	echo 'delete table inet filter' >> /etc/nftables.conf
	echo 'table inet filter {' >> /etc/nftables.conf
	echo '  chain input {' >> /etc/nftables.conf
	echo '    type filter hook input priority filter' >> /etc/nftables.conf
	echo '    policy drop' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '    ct state invalid drop comment "early drop of invalid connections"' >> /etc/nftables.conf
	echo '    ct state {established, related} accept comment "allow tracked connections"' >> /etc/nftables.conf
	echo '    iifname lo accept comment "allow from loopback"' >> /etc/nftables.conf
	echo '    ip protocol icmp accept comment "allow icmp"' >> /etc/nftables.conf
	echo '    meta l4proto ipv6-icmp accept comment "allow icmp v6"' >> /etc/nftables.conf
	echo '    tcp dport ssh accept comment "allow sshd"' >> /etc/nftables.conf
	echo '    pkttype host limit rate 5/second counter reject with icmpx type admin-prohibited' >> /etc/nftables.conf
	echo '    counter' >> /etc/nftables.conf
	echo '  }' >> /etc/nftables.conf
	echo '  chain forward {' >> /etc/nftables.conf
	echo '    type filter hook forward priority filter' >> /etc/nftables.conf
	echo '    policy drop' >> /etc/nftables.conf
	echo '  }' >> /etc/nftables.conf
	echo '}' >> /etc/nftables.conf

	uuid1=$(blkid -o value -s UUID /dev/${drive}p1)
	uuid2=$(blkid -o value -s UUID /dev/${drive}p2)
	echo '# <file system> <dir>     <type> <options>             <dump> <pass>' > /etc/fstab
	echo 'tmpfs           /tmp      tmpfs  defaults,nosuid,nodev 0      0' >> /etc/fstab
	echo "UUID=${uuid1}   /boot/efi vfat   defaults              0      2" >> /etc/fstab
	echo "UUID=${uuid2}   /         ext4   defaults              0      1" >> /etc/fstab
	echo '/swapfile       none      swap   defaults              0      0' >> /etc/fstab

	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 0600 /swapfile
	mkswap /swapfile > /dev/null

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done
	grub2-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB 2> /dev/null
	grub2-mkconfig -o /boot/grub2/grub.cfg 2> /dev/null

	useradd -m -G wheel -s /bin/bash ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	echo '* hard nofile 1048576' > /etc/security/limits.conf

	echo 'kernel.core_pattern=/dev/null' > /etc/sysctl.d/50-coredump.conf

	sed -i 's/#SystemMaxUse=/SystemMaxUse=50M/' /etc/systemd/journald.conf

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git
	sudo -u ${user} cp -R ${userpath}/git/${configdirectory}/. ${userpath}/
	cp -R ${userpath}/git/${configdirectory}/. ~root/
	sudo -u ${user} curl -LO https://download.damieng.com/fonts/original/EnvyCodeR-PR7.zip
	sudo -u ${user} unzip EnvyCodeR-PR7 > /dev/null
	sudo -u ${user} mkdir -p ${userpath}/.local/share/fonts/'Envy Code R'
	sudo -u ${user} cp -R ${userpath}/git/'Envy Code R PR7'/*.ttf ${userpath}/.local/share/fonts/'Envy Code R'/
	find ${userpath}/git -delete

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null

	dracut --force --regenerate-all

	fixfiles -F onboot

	exit
fi
