#!/bin/bash

drive='sda'
timezone='Europe/Lisbon'
localization='en_US.UTF-8'
hostname='vaio'
swapsize='2048'
configdirectory='Configurations/home'
install='
	base-system chrony curl git grub xdg-user-dirs
	xorg-minimal wireguard-tools
	gnome neovim xsel firefox mpv yt-dlp
	noto-fonts-ttf-extra noto-fonts-cjk noto-fonts-emoji
'
nonfree='
	intel-ucode nvidia390
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	dd if=/dev/zero of=/dev/${drive} bs=440 count=1 2> /dev/null
	sgdisk --zap-all /dev/${drive} > /dev/null 2> /dev/null
	parted -s /dev/${drive} mklabel msdos mkpart primary btrfs 1MiB 100% set 1 boot on
	mkfs.btrfs -f /dev/${drive}1 > /dev/null 2> /dev/null
	mount /dev/${drive}1 /mnt

	curl -LO https://alpha.de.repo.voidlinux.org/live/current/void-x86_64-ROOTFS-20210930.tar.xz
	tar xpf void-x86_64-ROOTFS-20210930.tar.xz --xattrs-include='*.*' --numeric-owner -C /mnt

	mount --types proc /proc /mnt/proc
	mount --rbind /sys /mnt/sys
	mount --make-rslave /mnt/sys
	mount --rbind /dev /mnt/dev
	mount --make-rslave /mnt/dev
	cp --dereference /etc/resolv.conf /mnt/etc/
	cp $0 /mnt/setup
	chroot /mnt /bin/bash ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	xbps-install -Suy
	xbps-install -uy
	xbps-install -y ${install}
	xbps-install -y void-repo-nonfree
	xbps-install -Sy
	xbps-install -y ${nonfree}
	xbps-remove -y base-voidstrap

	echo "${hostname}" > /etc/hostname
	sed -i -e '/HARDWARECLOCK/s/^#//g' -e 's/#KEYMAP="es"/KEYMAP="us"/' /etc/rc.conf
	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime

	echo "LANG=${localization}" > /etc/locale.conf
	echo 'LC_COLLATE=C' >> /etc/locale.conf

	xbps-reconfigure -f glibc-locales

	echo 'VISUAL=nvim' >> /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment

	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	ln -s /etc/sv/bluetoothd /etc/runit/runsvdir/default/
	ln -s /etc/sv/chronyd /etc/runit/runsvdir/default/
	ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
	ln -s /etc/sv/gdm /etc/runit/runsvdir/default/
	ln -s /etc/sv/iptables /etc/runit/runsvdir/default/
	ln -s /etc/sv/ip6tables /etc/runit/runsvdir/default/
	ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/

	mkdir -p /etc/X11/xorg.conf.d
	echo 'Section "InputClass"' > /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Identifier "touchpad"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Driver "libinput"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  MatchIsTouchpad "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/30-touchpad.conf

	mkdir -p /etc/NetworkManager/conf.d
	echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
	echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

	echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
	echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf

	echo '*filter' > /etc/iptables/iptables.rules
	echo ':INPUT DROP [0:0]' >> /etc/iptables/iptables.rules
	echo ':FORWARD DROP [0:0]' >> /etc/iptables/iptables.rules
	echo ':OUTPUT ACCEPT [0:0]' >> /etc/iptables/iptables.rules
	echo '-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -i lo -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p icmp -m icmp --icmp-type 3 -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p icmp -m icmp --icmp-type 11 -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p icmp -m icmp --icmp-type 12 -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p tcp -m tcp --dport 113 --tcp-flags FIN,SYN,RST,ACK SYN -j REJECT --reject-with tcp-reset' >> /etc/iptables/iptables.rules
	echo 'COMMIT' >> /etc/iptables/iptables.rules

	echo '*raw' > /etc/iptables/ip6tables.rules
	echo ':PREROUTING ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':OUTPUT ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo 'COMMIT' >> /etc/iptables/ip6tables.rules
	echo '*mangle' >> /etc/iptables/ip6tables.rules
	echo ':PREROUTING ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':INPUT ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':FORWARD ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':OUTPUT ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':POSTROUTING ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo 'COMMIT' >> /etc/iptables/ip6tables.rules
	echo '*filter' >> /etc/iptables/ip6tables.rules
	echo ':INPUT DROP [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':FORWARD DROP [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':OUTPUT ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -i lo -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -m conntrack --ctstate INVALID -j DROP' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -s fe80::/10 -p ipv6-icmp -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p udp -m conntrack --ctstate NEW -j REJECT --reject-with icmp6-port-unreachable' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j REJECT --reject-with tcp-reset' >> /etc/iptables/ip6tables.rules
	echo 'COMMIT' >> /etc/iptables/ip6tables.rules

	uuid1=$(blkid -o value -s UUID /dev/${drive}1)
	echo '# <device>            <dir>     <type> <options>             <dump> <fsck>' > /etc/fstab
	echo "UUID=${uuid1}         /         btrfs  defaults              0      1" >> /etc/fstab
	echo 'tmpfs                 /tmp      tmpfs  defaults,nosuid,nodev 0      0' >> /etc/fstab
	truncate -s 0 /swapfile
	chattr +C /swapfile
	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 600 /swapfile
	mkswap /swapfile > /dev/null
	echo '/swapfile             none      swap   rw,noatime,discard    0      0' >> /etc/fstab

	grub-install /dev/${drive} 2> /dev/null

	useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input,network,bluetooth ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git
	cp -a ${userpath}/git/${configdirectory}/. ${userpath}/
	find ${userpath}/git -delete

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null

	xbps-reconfigure -fa
	
	exit
fi
