#!/bin/bash

drive='nvme0n1'
timezone='Europe/Lisbon'
localization='en_US.UTF-8'
hostname='laptop'
swapsize='2048'
bootoptions='loglevel=4 nvidia-drm.modeset=1'
configdirectory='Configurations/home'
install='
	base-system chrony curl git grml-zsh-config zsh grub-x86_64-efi socklog-void wget xdg-user-dirs-gtk xdg-utils xtools
	xorg-minimal xf86-video-amdgpu mesa-vulkan-radeon mesa-vaapi mesa-vdpau alsa-plugins-pulseaudio nftables wireguard-tools
	gnome libvirt qemu virt-manager btop neovim wl-clipboard xsel
	firefox flatpak mpv yt-dlp qbittorrent telegram-desktop
	noto-fonts-cjk noto-fonts-emoji noto-fonts-ttf noto-fonts-ttf-extra
'
multilib='
	nvidia nvtop libgcc-32bit libstdc++-32bit libdrm-32bit libglvnd-32bit mesa-dri-32bit nvidia-libs-32bit pulseaudio-32bit
	alsa-plugins-pulseaudio-32bit wine wine-32bit wine-gecko wine-mono
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	wipefs --force --all /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 551MiB mkpart primary btrfs 551MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.btrfs -f /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mkdir -p /mnt/boot/efi
	mount /dev/${drive}p1 /mnt/boot/efi

	TAR=$(curl -s https://repo-default.voidlinux.org/live/current/ | grep -oP '(?<=").*x86_64-ROOTFS.*(?=")')
	wget https://repo-default.voidlinux.org/live/current/"$TAR"
	tar xpf "$TAR" --xattrs-include='*.*' --numeric-owner -C /mnt

	mount --types proc /proc /mnt/proc
	mount --rbind /sys /mnt/sys
	mount --make-rslave /mnt/sys
	mount --rbind /dev /mnt/dev
	mount --make-rslave /mnt/dev
	cp --dereference /etc/resolv.conf /mnt/etc/
	cp $0 /mnt/setup
	chroot /mnt /bin/bash ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	xbps-install -Suy xbps
	xbps-install -uy
	xbps-install -y ${install}
	xbps-install -y void-repo-nonfree void-repo-multilib{,-nonfree}
	xbps-install -Sy
	xbps-install -y ${multilib}
	xbps-remove -y base-voidstrap

	echo "${hostname}" > /etc/hostname
	sed -i -e '/HARDWARECLOCK/s/^#//g' -e 's/#KEYMAP="es"/KEYMAP="es"/' /etc/rc.conf
	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime

	echo "LANG=${localization}" > /etc/locale.conf
	echo 'LC_COLLATE=C' >> /etc/locale.conf

	xbps-reconfigure -f glibc-locales

	echo 'VISUAL=nvim' > /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment
	echo 'MOZ_ENABLE_WAYLAND=1' >> /etc/environment

	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	ln -s /etc/sv/bluetoothd /etc/runit/runsvdir/default/
	ln -s /etc/sv/chronyd /etc/runit/runsvdir/default/
	ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
	ln -s /etc/sv/gdm /etc/runit/runsvdir/default/
	ln -s /etc/sv/libvirt-generic /etc/runit/runsvdir/default/
	ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/
	ln -s /etc/sv/nftables /etc/runit/runsvdir/default/
	ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/
	ln -s /etc/sv/nanoklogd /etc/runit/runsvdir/default/

	mkdir -p /etc/X11/xorg.conf.d
	echo 'Section "Device"' > /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Identifier "AMD"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Driver "amdgpu"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "SWcursor" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "Accel" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  # Option "ZaphodHeads" ""' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "DRI" "3"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "EnablePageFlip" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "TearFree" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "VariableRefresh" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "AccelMethod" "glamor"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "ShadowPrimary" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/20-amdgpu.conf

	echo 'Section "InputClass"' > /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Identifier "touchpad"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Driver "libinput"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  MatchIsTouchpad "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/30-touchpad.conf

	mkdir -p /etc/NetworkManager/conf.d
	echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
	echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

	echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
	echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf

	echo '#!/usr/bin/nft -f' > /etc/nftables.conf
	echo '# vim:set ts=2 sw=2 et:' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '# IPv4/IPv6 Simple & Safe firewall ruleset.' >> /etc/nftables.conf
	echo '# More examples in /usr/share/nftables/ and /usr/share/doc/nftables/examples/.' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'table inet filter' >> /etc/nftables.conf
	echo 'delete table inet filter' >> /etc/nftables.conf
	echo 'table inet filter {' >> /etc/nftables.conf
	echo '  chain input {' >> /etc/nftables.conf
	echo '    type filter hook input priority filter' >> /etc/nftables.conf
	echo '    policy drop' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '    ct state invalid drop comment "early drop of invalid connections"' >> /etc/nftables.conf
	echo '    ct state {established, related} accept comment "allow tracked connections"' >> /etc/nftables.conf
	echo '    iifname lo accept comment "allow from loopback"' >> /etc/nftables.conf
	echo '    ip protocol icmp accept comment "allow icmp"' >> /etc/nftables.conf
	echo '    meta l4proto ipv6-icmp accept comment "allow icmp v6"' >> /etc/nftables.conf
	echo '    tcp dport ssh accept comment "allow sshd"' >> /etc/nftables.conf
	echo '    pkttype host limit rate 5/second counter reject with icmpx type admin-prohibited' >> /etc/nftables.conf
	echo '    counter' >> /etc/nftables.conf
	echo '  }' >> /etc/nftables.conf
	echo '  chain forward {' >> /etc/nftables.conf
	echo '    type filter hook forward priority filter' >> /etc/nftables.conf
	echo '    policy drop' >> /etc/nftables.conf
	echo '  }' >> /etc/nftables.conf
	echo '}' >> /etc/nftables.conf

	echo '* hard nofile 1048576' > /etc/security/limits.conf

	uuid1=$(blkid -o value -s UUID /dev/${drive}p1)
	uuid2=$(blkid -o value -s UUID /dev/${drive}p2)
	echo '# <file system> <dir>     <type> <options>             <dump> <pass>' > /etc/fstab
	echo 'tmpfs           /tmp      tmpfs  defaults,nosuid,nodev 0      0' >> /etc/fstab
	echo "UUID=${uuid1}   /boot/efi vfat   defaults              0      2" >> /etc/fstab
	echo "UUID=${uuid2}   /         btrfs  defaults              0      1" >> /etc/fstab
	echo '/swapfile       none      swap   rw,noatime,discard    0      0' >> /etc/fstab

	truncate -s 0 /swapfile
	chattr +C /swapfile
	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 600 /swapfile
	mkswap /swapfile > /dev/null

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done
	grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB 2> /dev/null
	sed -i "s/loglevel=4/${bootoptions}/" /etc/default/grub
	grub-mkconfig -o /boot/grub/grub.cfg 2> /dev/null

	chsh -s /bin/zsh > /dev/null
	useradd -m -s /bin/zsh -U -G wheel,users,audio,video,cdrom,input,network,bluetooth,libvirt,kvm,socklog ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git
	cp -a ${userpath}/git/${configdirectory}/. ${userpath}/
	find ${userpath}/git -delete

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null

	mkdir -p /etc/udev/rules.d
	ln -s /dev/null /etc/udev/rules.d/61-gdm.rules

	xbps-reconfigure -fa

	exit
fi
