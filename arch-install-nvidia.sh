#!/usr/bin/bash

drive='nvme0n1'
timezone='Europe/Lisbon'
mirror='pt,gb'
localization='en_US.UTF-8'
keyboardlayout='us'
hostname='lenovo'
swapsize='2048'
bootoptions='quiet loglevel=3 systemd.show_status=auto rd.udev.log_level=3 nmi_watchdog=0 nvidia_drm.modeset=1'
configdirectory='Configurations/home'
services='bluetooth fstrim.timer gdm iptables ip6tables NetworkManager openntpd reflector.timer'
install='
	base base-devel linux-zen dkms linux-zen-headers linux-firmware amd-ucode reflector
	bash bash-completion curl git openntpd pacman-contrib pv wget xdg-user-dirs dosfstools e2fsprogs efibootmgr parted fwupd udisks2 xfsprogs
	xorg-server xorg-apps xf86-video-amdgpu mesa libva-mesa-driver mesa-vdpau
	vulkan-icd-loader nvidia-dkms nvidia-utils nvidia-settings nvidia-prime
	bluez bluez-utils networkmanager iptables openresolv wireguard-tools
	pipewire pipewire-audio pipewire-alsa pipewire-pulse pipewire-jack wireplumber realtime-privileges
	gnome gnome-extra
	flatpak xdg-desktop-portal xdg-desktop-portal-gnome htop kitty man-db neovim wl-clipboard xsel nvtop weechat
	bandwhich chromium firefox qutebrowser python-adblock mpv yt-dlp qbittorrent telegram-desktop
	noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra ttf-cascadia-code ttf-nerd-fonts-symbols
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done

	rm -f /sys/firmware/efi/efivars/dump-*

	wipefs -fa /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 1025MiB mkpart primary xfs 1025MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.xfs -f /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mount --mkdir /dev/${drive}p1 /mnt/boot

	reflector --country ${mirror} --protocol https --latest 5 --sort rate --save /etc/pacman.d/mirrorlist

	sed -i -e '/Color/s/^#//g' -e '/ParallelDownloads/s/^#//g' /etc/pacman.conf

	pacstrap -K /mnt ${install}

	genfstab -U /mnt >> /mnt/etc/fstab

	cp $0 /mnt/setup
	arch-chroot /mnt ./setup ${user} ${userpassword} ${rootpassword}
	rm -f /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	hwclock --systohc

	sed -i "/${localization}/s/^#//g" /etc/locale.gen
	locale-gen > /dev/null
	echo "LANG=${localization}" > /etc/locale.conf
	echo "KEYMAP=${keyboardlayout}" > /etc/vconsole.conf

	echo "${hostname}" > /etc/hostname

	echo '127.0.0.1 localhost' > /etc/hosts
	echo "127.0.1.1 ${hostname}" >> /etc/hosts
	echo '' >> /etc/hosts
	echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
	echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
	echo 'fe00::0 ip6-localnet' >> /etc/hosts
	echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
	echo 'ff02::1 ip6-allnodes' >> /etc/hosts
	echo 'ff02::2 ip6-allrouters' >> /etc/hosts
	echo 'ff02::3 ip6-allhosts' >> /etc/hosts

	echo 'VISUAL=nvim' >> /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment

	sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	systemctl stop systemd-timesyncd 2> /dev/null
	systemctl disable systemd-timesyncd 2> /dev/null
	systemctl mask systemd-timesyncd 2> /dev/null
	systemctl enable ${services} 2> /dev/null

	echo 'Section "OutputClass"' > /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Identifier "AMDgpu"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  MatchDriver "amdgpu"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Driver "amdgpu"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "HotplugDriver" "amdgpu"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "SWcursor" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "Accel" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  # Option "ZaphodHeads" ""' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "DRI" "3"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "EnablePageFlip" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "TearFree" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "VariableRefresh" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "AsyncFlipSecondaries" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "AccelMethod" "glamor"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "ShadowPrimary" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/20-amdgpu.conf

	echo 'Section "InputClass"' > /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Identifier "touchpad"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Driver "libinput"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  MatchIsTouchpad "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Option "NaturalScrolling" "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Option "Tapping" "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/30-touchpad.conf

	echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
	echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf
	echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
	echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

	echo '*filter' > /etc/iptables/iptables.rules
	echo ':INPUT DROP [0:0]' >> /etc/iptables/iptables.rules
	echo ':FORWARD DROP [0:0]' >> /etc/iptables/iptables.rules
	echo ':OUTPUT ACCEPT [0:0]' >> /etc/iptables/iptables.rules
	echo ':TCP - [0:0]' >> /etc/iptables/iptables.rules
	echo ':UDP - [0:0]' >> /etc/iptables/iptables.rules
	echo '-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -i lo -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -m conntrack --ctstate INVALID -j DROP' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p icmp -m icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p udp -m conntrack --ctstate NEW -j UDP' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable' >> /etc/iptables/iptables.rules
	echo '-A INPUT -p tcp -j REJECT --reject-with tcp-reset' >> /etc/iptables/iptables.rules
	echo '-A INPUT -j REJECT --reject-with icmp-proto-unreachable' >> /etc/iptables/iptables.rules
	echo 'COMMIT' >> /etc/iptables/iptables.rules

	echo '*filter' > /etc/iptables/ip6tables.rules
	echo ':INPUT DROP [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':FORWARD DROP [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':OUTPUT ACCEPT [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':TCP - [0:0]' >> /etc/iptables/ip6tables.rules
	echo ':UDP - [0:0]' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -i lo -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -m conntrack --ctstate INVALID -j DROP' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -s fe80::/10 -p ipv6-icmp -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p udp --sport 547 --dport 546 -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p udp -m conntrack --ctstate NEW -j UDP' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p udp -j REJECT --reject-with icmp6-adm-prohibited' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p tcp -j REJECT --reject-with tcp-reset' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -j REJECT --reject-with icmp6-adm-prohibited' >> /etc/iptables/ip6tables.rules
	echo '-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -m conntrack --ctstate NEW -j ACCEPT' >> /etc/iptables/ip6tables.rules
	echo 'COMMIT' >> /etc/iptables/ip6tables.rules

	echo 'ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/scheduler}="none"' > /etc/udev/rules.d/60-ioschedulers.rules

	echo 'kernel.core_pattern=/dev/null' > /etc/sysctl.d/50-coredump.conf

	echo 'MODULES=(amdgpu nvidia nvidia_modeset nvidia_uvm nvidia_drm)' > /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'BINARIES=()' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'FILES=()' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'HOOKS=(base systemd autodetect modconf block filesystems fsck)' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'COMPRESSION="lz4"' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'COMPRESSION_OPTIONS=(-9)' >> /etc/mkinitcpio.conf.d/myhooks.conf
	echo 'MODULES_DECOMPRESS="yes"' >> /etc/mkinitcpio.conf.d/myhooks.conf

	echo '--save /etc/pacman.d/mirrorlist' > /etc/xdg/reflector/reflector.conf
	echo "--country ${mirror}" >> /etc/xdg/reflector/reflector.conf
	echo '--protocol https' >> /etc/xdg/reflector/reflector.conf
	echo '--latest 5' >> /etc/xdg/reflector/reflector.conf
	echo '--sort rate' >> /etc/xdg/reflector/reflector.conf

	ln -s /dev/null /etc/udev/rules.d/61-gdm.rules

	sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf

	sed -i -e '/RuntimeWatchdogSec/s/^#//g' -e 's/#RebootWatchdogSec=10min/RebootWatchdogSec=off/' -e '/KExecWatchdogSec/s/^#//g' /etc/systemd/system.conf

	sed -i 's/#DefaultLimitNOFILE=1024:524288/DefaultLimitNOFILE=1024:1048576/' /etc/systemd/system.conf
	sed -i 's/#DefaultLimitNOFILE=/DefaultLimitNOFILE=1024:1048576/' /etc/systemd/user.conf

	sed -i 's/#SystemMaxUse=/SystemMaxUse=50M/' /etc/systemd/journald.conf

	sed -i -e '/Color/s/^#//g' -e '/ParallelDownloads/s/^#//g' /etc/pacman.conf

	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 0600 /swapfile
	mkswap -U clear /swapfile > /dev/null
	echo '# /swapfile' >> /etc/fstab
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab

	bootctl install 2> /dev/null
	uuidroot=$(blkid -o value -s UUID /dev/${drive}p2)
	echo 'title   Arch Linux' > /boot/loader/entries/arch.conf
	echo 'linux   /vmlinuz-linux-zen' >> /boot/loader/entries/arch.conf
	echo 'initrd  /amd-ucode.img' >> /boot/loader/entries/arch.conf
	echo 'initrd  /initramfs-linux-zen.img' >> /boot/loader/entries/arch.conf
	echo "options root=UUID=${uuidroot} rw ${bootoptions}" >> /boot/loader/entries/arch.conf

	useradd -m -G wheel,realtime -s /usr/bin/bash ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git 2> /dev/null
	sudo -u ${user} cp -R ${userpath}/git/${configdirectory}/. ${userpath}/
	cp -R ${userpath}/git/${configdirectory}/. ~root/
	sudo -u ${user} git clone https://aur.archlinux.org/paru-bin.git 2> /dev/null
	cd paru-bin
	sudo -u ${user} makepkg -sirc --noconfirm

	printf "${rootpassword}\n${rootpassword}\n" | passwd > /dev/null 2> /dev/null
	printf "${userpassword}\n${userpassword}\n" | passwd ${user} > /dev/null 2> /dev/null

	mkinitcpio -P
fi
