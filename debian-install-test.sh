#!/bin/bash

drive='nvme0n1'
timezone='Europe/Lisbon'
mirror='http://deb.debian.org/debian'
localization='en_US.UTF-8'
keyboardlayout='us'
hostname='pc'
swapsize='2048'
configdirectory='Configurations/home'
install='
	linux-image-amd64 firmware-linux locales sudo curl git wget
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done

	rm -f /sys/firmware/efi/efivars/dump-*

	wipefs --force --all /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 551MiB mkpart primary ext4 551MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.ext4 -F /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mkdir -p /mnt/boot/efi
	mount /dev/${drive}p1 /mnt/boot/efi

	pacman -Sy --needed --noconfirm archlinux-keyring debootstrap
	debootstrap sid /mnt ${mirror}

	genfstab -U /mnt >> /mnt/etc/fstab

	cp $0 /mnt/setup
	arch-chroot /mnt ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	echo 'deb http://deb.debian.org/debian sid main contrib non-free' > /etc/apt/sources.list
	echo 'deb-src http://deb.debian.org/debian sid main contrib non-free' >> /etc/apt/sources.list

	export DEBIAN_FRONTEND=noninteractive
	apt-get update
	apt-get install -y ${install}

	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	hwclock --systohc

	sed -i "/${localization}/s/^#//g" /etc/locale.gen
	locale-gen > /dev/null
	echo "LANG=${localization}" > /etc/locale.conf

	echo "KEYMAP=${keyboardlayout}" > /etc/vconsole.conf

	echo "${hostname}" > /etc/hostname

	echo '127.0.0.1 localhost' > /etc/hosts
	echo "127.0.1.1 ${hostname}" >> /etc/hosts
	echo '' >> /etc/hosts
	echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
	echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
	echo 'fe00::0 ip6-localnet' >> /etc/hosts
	echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
	echo 'ff02::1 ip6-allnodes' >> /etc/hosts
	echo 'ff02::2 ip6-allrouters' >> /etc/hosts
	echo 'ff02::3 ip6-allhosts' >> /etc/hosts

	sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 600 /swapfile
	mkswap /swapfile > /dev/null
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab

	uuid=$(blkid -o value -s UUID /dev/${drive}p2)
	echo "fs0:\vmlinuz root=${uuid} initrd=initrd.img" > /mnt/boot/efi/startup.nsh

	cp -f /vmlinuz /initrd.img /boot/efi
	echo '#!/bin/sh' > /etc/kernel/postinst.d/zz-update-efistub
	echo 'cp -f /vmlinuz /initrd.img /boot/efi' >> /etc/kernel/postinst.d/zz-update-efistub
	chmod +x /etc/kernel/postinst.d/zz-update-efistub

	useradd -m -G sudo -s /bin/bash ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null

	apt-get clean
fi
