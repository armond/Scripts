#!/bin/bash

set -euo pipefail

readonly target_dir="/mnt"
readonly drive="/dev/nvme0n1"
readonly esp_partition="${drive}p1"
readonly root_partition="${drive}p2"
readonly cryptname="cryptroot"
readonly esp_mount="${target_dir}/boot"
readonly swap_size="4G"
readonly timezone="Europe/Lisbon"
readonly localization="en_US.UTF-8"
readonly keyboard_layout="us"
readonly hostname="desktop"
readonly boot_options="quiet loglevel=3 nmi_watchdog=0"
readonly config_directory="Configurations/home"
readonly install_pkgs="
  base base-devel cryptsetup linux-zen linux-firmware amd-ucode
  arch-install-scripts bash bash-completion chrony curl git linux-tools pacman-contrib wget xdg-user-dirs
  dosfstools e2fsprogs efibootmgr fuse2 parted fwupd udisks2
  xorg-server xorg-apps xorg-xinit xf86-video-amdgpu mesa
  vulkan-icd-loader vulkan-radeon vulkan-tools
  bluez bluez-utils networkmanager nftables openresolv wireguard-tools
  pipewire pipewire-audio pipewire-alsa pipewire-pulse pipewire-jack wireplumber realtime-privileges
  plasma-meta gnome-themes-extra xdg-desktop-portal xdg-desktop-portal-kde ark dolphin dolphin-plugins
  gwenview kate kcalc kdiff3 kjournald konsole konversation krusader ksystemlog okular spectacle
  bandwhich ghostty htop lutris man-db man-pages texinfo neovim wl-clipboard xsel nfoview nvtop weechat
  chromium firefox qutebrowser python-adblock mpv yt-dlp qbittorrent telegram-desktop
  noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra ttf-cascadia-code ttf-liberation
"
readonly multilib_pkgs="
  lib32-mesa lib32-vulkan-icd-loader lib32-vulkan-radeon
  lib32-pipewire lib32-libpulse lib32-alsa-lib lib32-alsa-plugins
  steam gamemode lib32-gamemode
  wine-staging wine-gecko wine-mono winetricks
"

declare username=""
declare user_password=""
declare root_password=""
declare luks_password=""

cleanup() {
  local exit_code=$?

  unset user_password root_password luks_password 2>/dev/null || true

  if mountpoint -q "${esp_mount}"; then
    umount -f "${esp_mount}" 2>/dev/null || true
  fi

  if mountpoint -q "${target_dir}"; then
    umount -Rf "${target_dir}" 2>/dev/null || true
  fi

  if cryptsetup status "${cryptname}" &>/dev/null; then
    cryptsetup close "${cryptname}" 2>/dev/null || true
  fi

  exit $exit_code
}

trap 'cleanup' ERR EXIT SIGINT SIGTERM SIGHUP

check_interactive() {
  if ! [[ -t 0 && -t 1 ]]; then
    echo "ERROR: Run interactively (e.g., ./script.sh, not via cron/pipe)." > /dev/tty
    exit 1
  fi
}

check_root() {
  if [[ "${EUID}" -ne 0 ]]; then
    cat <<EOF > /dev/tty
ERROR: This script requires root privileges.
       Re-run with either:
       1. Direct root login: sudo -i
       2. Execute with sudo: sudo $0
EOF
    exit 1
  fi
}

check_efi() {
  if [[ ! -d /sys/firmware/efi ]]; then
    echo "ERROR: Not booted in UEFI mode!" >&2
    exit 1
  fi
}

check_cmd() {
  for cmd in cryptsetup curl efibootmgr tar xz; do
    command -v "${cmd}" >/dev/null || {
      echo "Required command missing: ${cmd}" >&2
      exit 1
    }
  done
}

get_credentials() {
  read -rp "Enter username: " username < /dev/tty
  read -rp "Enter password for ${username}: " user_password < /dev/tty
  read -rp "Enter root password: " root_password < /dev/tty
  read -rp "Enter LUKS encryption password: " luks_password < /dev/tty
}

validate_variables() {
  local missing=()
  [[ -z "${username}" ]] && missing+=("username")
  [[ -z "${user_password}" ]] && missing+=("user password")
  [[ -z "${root_password}" ]] && missing+=("root password")
  [[ -z "${luks_password}" ]] && missing+=("LUKS password")

  if (( ${#missing[@]} > 0 )); then
    echo "ERROR: Missing required values for:" >&2
    printf '  - %s\n' "${missing[@]}" >&2
    exit 1
  fi
}

clear_efi() {
  echo "Clearing existing UEFI entries..."
  {
    efibootmgr 2>/dev/null || true
    efibootmgr | (grep '^Boot[0-9]' || true) | while read -r line; do
      bootnum="${line%% *}"
      bootnum="${bootnum#Boot}"
      efibootmgr -B -b "${bootnum}" 2>/dev/null || true
    done
  } || echo "Warning: Failed to clear UEFI entries (non-critical)" >&2

  rm -f /sys/firmware/efi/efivars/dump-* 2>/dev/null || true
}

update_system_clock() {
  timedatectl set-ntp true
  timedatectl set-timezone "${timezone}"
}

partition_disk() {
  echo "Partitioning disk ${drive}..."
  parted -s "${drive}" mklabel gpt \
    mkpart ESP fat32 1MiB 1025MiB \
    set 1 esp on \
    mkpart primary ext4 1025MiB 100%
  partprobe "${drive}"
}

format_partitions() {
  echo "Formatting partitions..."
  mkfs.fat -F32 "${esp_partition}"
  printf "%s" "${luks_password}" | cryptsetup luksFormat --type luks2 --batch-mode \
    --pbkdf argon2id --hash blake2b-512 --key-file=- "${root_partition}"
  printf "%s" "${luks_password}" | cryptsetup open --allow-discards --persistent \
    --key-file=- "${root_partition}" "${cryptname}"
  mkfs.ext4 -F "/dev/mapper/${cryptname}"
}

mount_filesystems() {
  echo "Mounting filesystems..."
  mount "/dev/mapper/${cryptname}" "${target_dir}"
  mkdir -p "${esp_mount}"
  mount "${esp_partition}" "${esp_mount}"
}

install_base() {
  mkdir -p /etc/pacman.d
  echo 'Server = https://gb.mirrors.cicku.me/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

  sed -i '/^#Color/s/#//' /etc/pacman.conf

  pacstrap -K /mnt ${install_pkgs}
}

configure_chroot() {
  trap - ERR EXIT SIGINT SIGTERM SIGHUP

  source /tmp/install_vars.sh

  mkdir -p /etc/pacman.d
  echo 'Server = https://gb.mirrors.cicku.me/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

  sed -i -e '/^#Color/s/#//' -e '/^#\[multilib\]/,/^#Include/s/^#//' /etc/pacman.conf

  pacman -Syu --noconfirm
  pacman -S ${multilib_pkgs} --noconfirm

  ln -sf "/usr/share/zoneinfo/${timezone}" /etc/localtime
  hwclock --systohc

  sed -i "/^#${localization}/s/#//" /etc/locale.gen
  echo "LANG=${localization}" > /etc/locale.conf
  locale-gen

  {
  echo "KEYMAP=${keyboard_layout}"
  echo 'FONT=Lat2-Terminus16'
  } > /etc/vconsole.conf

  echo "${hostname}" > /etc/hostname

  {
    echo '127.0.0.1 localhost'
    echo "127.0.1.1 ${hostname}"
    echo
    echo '# The following lines are desirable for IPv6 capable hosts'
    echo '::1     ip6-localhost ip6-loopback'
    echo 'fe00::0 ip6-localnet'
    echo 'ff00::0 ip6-mcastprefix'
    echo 'ff02::1 ip6-allnodes'
    echo 'ff02::2 ip6-allrouters'
    echo 'ff02::3 ip6-allhosts'
  } > /etc/hosts

  {
    echo 'VISUAL=nvim'
    echo 'EDITOR=nvim'
    echo 'POWERDEVIL_NO_DDCUTIL=1'
  } >> /etc/environment

  mkdir -p /etc/sudoers.d
  echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel

  for action in disable mask; do systemctl "$action" systemd-timesyncd; done
  for service in bluetooth chronyd fstrim.timer NetworkManager nftables paccache.timer sddm; do systemctl enable "$service"; done

  mkdir -p /etc/NetworkManager/conf.d
  {
    echo '[keyfile]'
    echo 'unmanaged-devices=type:wireguard'
  } > /etc/NetworkManager/conf.d/unmanaged.conf

  {
    echo '[main]'
    echo 'rc-manager=resolvconf'
  } > /etc/NetworkManager/conf.d/rc-manager.conf

  {
    echo '#!/usr/bin/nft -f'
    echo
    echo 'flush ruleset'
    echo
    echo 'table inet filter {'
    echo '  chain input {'
    echo '    type filter hook input priority 0;'
    echo
    echo '    # accept any localhost traffic'
    echo '    iif lo accept'
    echo
    echo '    # accept traffic originated from us'
    echo '    ct state established,related accept'
    echo
    echo '    # activate to accept common local services'
    echo '    #tcp dport { 22, 80, 443 } ct state new accept'
    echo
    echo '    # ICMPv6 exceptions (RFC4890)'
    echo '    meta nfproto ipv6 icmpv6 type { destination-unreachable, '\
  'packet-too-big, time-exceeded, parameter-problem, echo-reply, '\
  'echo-request, nd-router-solicit, nd-router-advert, '\
  'nd-neighbor-solicit, nd-neighbor-advert, 148, 149 } accept'
    echo '    ip6 saddr fe80::/10 icmpv6 type { 130, 131, 132, 143, 151, 152, '\
  '153 } accept'
    echo
    echo '    # count and drop any other traffic'
    echo '    counter drop'
    echo '  }'
    echo '}'
  } > /etc/nftables.conf

  mkdir -p /etc/mkinitcpio.conf.d
  {
    echo 'MODULES=(amdgpu)'
    echo 'BINARIES=()'
    echo 'FILES=()'
    echo 'HOOKS=(base udev autodetect microcode modconf keyboard keymap consolefont block encrypt filesystems fsck)'
  } > /etc/mkinitcpio.conf.d/myhooks.conf

  mkdir -p /etc/sysctl.d
  echo 'kernel.split_lock_mitigate = 0' > /etc/sysctl.d/99-splitlock.conf

  mkdir -p /etc/systemd/coredump.conf.d
  {
    echo '[Coredump]'
    echo 'Storage=none'
    echo 'ProcessSizeMax=0'
  } > /etc/systemd/coredump.conf.d/coredump.conf

  mkdir -p /etc/systemd/journald.conf.d
  {
  echo '[Journal]'
  echo 'SystemMaxUse=50M'
  } > /etc/systemd/journald.conf.d/journald.conf

  mkdir -p /etc/systemd/system.conf.d
  {
    echo '[Manager]'
    echo 'RuntimeWatchdogSec=off'
    echo 'RebootWatchdogSec=off'
    echo 'KExecWatchdogSec=off'
    echo 'DefaultTimeoutStartSec=5s'
    echo 'DefaultTimeoutStopSec=5s'
    echo 'DefaultLimitNOFILE=1024:1048576'
  } > /etc/systemd/system.conf.d/system.conf

  mkdir -p /etc/systemd/user.conf.d
  {
    echo '[Manager]'
    echo 'RuntimeWatchdogSec=off'
    echo 'RebootWatchdogSec=off'
    echo 'KExecWatchdogSec=off'
    echo 'DefaultTimeoutStartSec=5s'
    echo 'DefaultTimeoutStopSec=5s'
    echo 'DefaultLimitNOFILE=1024:1048576'
  } > /etc/systemd/user.conf.d/user.conf

  mkdir -p /etc/sddm.conf.d
  {
    echo '[General]'
    echo 'Numlock=on'
    echo
    echo '[Theme]'
    echo 'Current=breeze'
  } > /etc/sddm.conf.d/sddm.conf

  mkdir -p /usr/share/icons/default
  {
    echo '[Icon Theme]'
    echo 'Inherits=breeze_cursors'
  } > /usr/share/icons/default/index.theme

  swapon --show=NAME | grep -q '/swapfile' && swapoff /swapfile
  rm -f /swapfile
  mkswap -U clear --size "${swap_size}" --file /swapfile
  swapon /swapfile
  genfstab -U / >> /etc/fstab
  swapoff /swapfile

  bootctl install
  {
    echo 'title   Arch Linux'
    echo 'linux   /vmlinuz-linux-zen'
    echo 'initrd  /initramfs-linux-zen.img'
    echo "options cryptdevice=UUID=${luks_uuid}:${cryptname} root=UUID=${root_uuid} rw ${boot_options}"
  } > /boot/loader/entries/arch.conf

  useradd -m -G wheel,realtime,gamemode -s /usr/bin/bash ${username}
  sudo -u "${username}" xdg-user-dirs-update
  userpath=$(getent passwd "${username}" | cut -d: -f6)

  sudo -u "${username}" mkdir -p "${userpath}/git"
  cd "${userpath}/git"
  sudo -u "${username}" git clone --depth 1 https://gitlab.com/armond/Configurations.git
  sudo -u "${username}" cp -r "${userpath}/git/${config_directory}/." "${userpath}/"
  cp -r "${userpath}/git/${config_directory}/." /root/

  mkinitcpio -p linux-zen

  printf "%s\n%s\n" "${user_password}" "${user_password}" | passwd "${username}" >/dev/null 2>&1
  printf "%s\n%s\n" "${root_password}" "${root_password}" | passwd root >/dev/null 2>&1

  rm -f /tmp/install_vars.sh
  rm -f "$0"

  exit 0
}

configure_system() {
  luks_uuid=$(blkid -s UUID -o value "${root_partition}")
  root_uuid=$(blkid -s UUID -o value "/dev/mapper/${cryptname}")

  for dir in dev proc sys; do
    mount --rbind "/$dir" "${target_dir}/$dir"
    mount --make-rslave "${target_dir}/$dir"
  done
  cp --dereference /etc/resolv.conf "${target_dir}/etc/"

  mkdir -p "${target_dir}/tmp"
  cat <<EOF > "${target_dir}/tmp/install_vars.sh"
export luks_uuid="${luks_uuid}"
export root_uuid="${root_uuid}"
export username="${username}"
export user_password="${user_password}"
export root_password="${root_password}"
EOF

  cp "$0" "${target_dir}/tmp/install.sh"
  chmod +x "${target_dir}/tmp/install.sh"

  chroot "${target_dir}" /bin/bash -c "/tmp/install.sh --chroot"
}

main() {
  if [[ "${1:-}" == "--chroot" ]]; then
    configure_chroot
  else
    check_interactive
    check_root
    check_efi
    check_cmd
    get_credentials
    validate_variables
    clear_efi
    update_system_clock
    partition_disk
    format_partitions
    mount_filesystems
    install_base
    configure_system
  fi
}

main "$@"
