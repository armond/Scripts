#!/bin/bash

set -euo pipefail

# Constants and configuration
readonly xbps_arch="x86_64"
readonly xbps_repo="https://repo-de.voidlinux.org"
readonly target_dir="/mnt"
readonly drive="/dev/nvme0n1"
readonly esp_partition="${drive}p1"
readonly root_partition="${drive}p2"
readonly esp_mount="${target_dir}/boot"
readonly swap_size="4G"
readonly timezone="Europe/Lisbon"
readonly localization="en_US.UTF-8"
readonly hostname="desktop"
readonly kernel="linux6.12"
readonly boot_options="quiet loglevel=3 console=tty2 nmi_watchdog=0"
readonly config_directory="Configurations/home"
readonly install_pkgs=(
  base-devel bash-completion chrony efibootmgr fuse fwupd linux-tools lz4 ngetty
  parted psmisc snooze socklog-void strace systemd-boot wget xdg-user-dirs xtools
  xorg-minimal xorg-apps xf86-video-amdgpu mesa-vulkan-radeon mesa-vaapi mesa-vdpau
  nftables wireguard-tools pipewire alsa-pipewire gstreamer1-pipewire libjack-pipewire
  libspa-bluetooth libspa-jack libspa-v4l2 rtkit kde-plasma kde-baseapps ark gwenview
  kcalc kdiff3 konversation krusader ksystemlog okular spectacle bluez NetworkManager
  bandwhich ghostty htop lutris neovim wl-clipboard xsel nvtop weechat chromium
  firefox qutebrowser python3-adblock mpv yt-dlp qbittorrent telegram-desktop
  nerd-fonts-ttf nerd-fonts-symbols-ttf noto-fonts-cjk noto-fonts-emoji noto-fonts-ttf
  noto-fonts-ttf-extra
)
readonly nonfree_pkgs=(
  libgcc-32bit libstdc++-32bit libdrm-32bit libglvnd-32bit mesa-dri-32bit
  vulkan-loader-32bit mesa-vulkan-radeon-32bit mesa-vaapi-32bit mesa-vdpau-32bit
  pipewire-32bit alsa-pipewire-32bit gstreamer1-pipewire-32bit libjack-pipewire-32bit
  libspa-bluetooth-32bit libspa-jack-32bit libspa-v4l2-32bit steam gamemode
  libgamemode-32bit wine wine-32bit wine-gecko wine-mono winetricks
)

# User credentials
declare username
declare user_password
declare root_password

# XBPS variables
declare xbps_version
declare xbps_url
declare xbps_hash

# Error handling
trap 'cleanup' ERR

cleanup() {
  echo "Error occurred. Cleaning up..." >&2
  umount -R "${target_dir}" 2>/dev/null || true
  exit 1
}

vercmp() {
  local a="$1" b="$2"
  local a_ver=() b_ver=()
  IFS='.' read -ra a_ver <<< "${a}"
  IFS='.' read -ra b_ver <<< "${b}"
  local len=$(( ${#a_ver[@]} > ${#b_ver[@]} ? ${#a_ver[@]} : ${#b_ver[@]} ))

  for ((i=0; i<len; i++)); do
    local a_num=${a_ver[i]:-0}
    local b_num=${b_ver[i]:-0}
    if (( a_num > b_num )); then
      echo 1
      return
    elif (( a_num < b_num )); then
      echo -1
      return
    fi
  done
  echo 0
}

get_latest_xbps() {
  local static_url="https://repo-default.voidlinux.org/static"
  local checksum_url="${static_url}/sha256sums.txt"
  local checksum_content

  echo "Fetching checksum data from ${checksum_url}..."

  if ! checksum_content=$(curl -sfL --retry 3 --max-time 30 "${checksum_url}"); then
    echo "ERROR: Failed to download sha256sums.txt" >&2
    exit 1
  fi

  local pattern="^([a-f0-9]{64})\\s+xbps-static-static-([0-9]+\\.[0-9.]+)_([0-9]+)\\.${xbps_arch}-musl\\.tar\\.xz$"
  local best_version="" best_revision="" best_hash=""

  while IFS= read -r line; do
    if [[ "${line}" =~ ${pattern} ]]; then
      local hash="${BASH_REMATCH[1]}"
      local version="${BASH_REMATCH[2]}"
      local revision="${BASH_REMATCH[3]}"

      if [[ -z "${best_version}" ]] ||
         [[ $(vercmp "${version}" "${best_version}") -gt 0 ]] ||
         { [[ $(vercmp "${version}" "${best_version}") -eq 0 ]] &&
           [[ "${revision}" -gt "${best_revision}" ]]; }; then
        best_version="${version}"
        best_revision="${revision}"
        best_hash="${hash}"
      fi
    fi
  done <<< "${checksum_content}"

  if [[ -z "${best_version}" ]]; then
    echo "ERROR: No valid packages found for ${xbps_arch}" >&2
    exit 1
  fi

  xbps_version="${best_version}_${best_revision}"
  xbps_url="${static_url}/xbps-static-static-${xbps_version}.${xbps_arch}-musl.tar.xz"
  xbps_hash="${best_hash}"

  echo "Resolved latest version: ${xbps_version}"
}

check_root() {
  if [[ "${EUID}" -ne 0 ]]; then
    echo "This script must be run as root" >&2
    exit 1
  fi
}

get_credentials() {
  read -rp "Enter username: " username
  read -rsp "Enter password for ${username}: " user_password
  echo
  read -rsp "Enter root password: " root_password
  echo
}

partition_disk() {
  echo "Partitioning disk ${drive}..."
  parted -s "${drive}" mklabel gpt
  parted -s "${drive}" mkpart ESP fat32 1MiB 1025MiB
  parted -s "${drive}" set 1 esp on
  parted -s "${drive}" mkpart primary ext4 1025MiB 100%
}

format_partitions() {
  echo "Formatting partitions..."
  mkfs.fat -F32 "${esp_partition}"
  mkfs.ext4 -F "${root_partition}"
}

mount_filesystems() {
  echo "Mounting filesystems..."
  mount "${root_partition}" "${target_dir}"
  mkdir -p "${esp_mount}"
  mount "${esp_partition}" "${esp_mount}"
}

install_base() {
  echo "Installing base system..."
  mkdir -p "${target_dir}/var/db/xbps/keys"
  cp /var/db/xbps/keys/* "${target_dir}/var/db/xbps/keys/"

  local xbps_tool=(XBPS_ARCH="${xbps_arch}" xbps-install -Sy -r "${target_dir}" -R "${xbps_repo}/current")
  "${xbps_tool[@]}" base-system "${kernel}"
}

xchroot() {
  local chroot_dir="$1"
  shift
  local cmd=("$@")

  [[ -d "${chroot_dir}" ]] || { echo "Chroot directory missing" >&2; return 1; }
  for dir in dev proc sys; do
    [[ -d "${chroot_dir}/${dir}" ]] || { echo "Missing /${dir} in chroot" >&2; return 1; }
  done

  for fs in dev proc sys; do
    mount --rbind "/${fs}" "${chroot_dir}/${fs}"
    mount --make-rslave "${chroot_dir}/${fs}"
  done

  touch "${chroot_dir}/etc/resolv.conf"
  mount --bind /etc/resolv.conf "${chroot_dir}/etc/resolv.conf"

  local cleanup() {
    umount -R "${chroot_dir}/dev" "${chroot_dir}/proc" "${chroot_dir}/sys" \
      "${chroot_dir}/etc/resolv.conf" 2>/dev/null || true
  }
  trap cleanup EXIT INT TERM

  local inner_shell
  if [[ -x "${chroot_dir}/${SHELL}" ]]; then
    inner_shell="${SHELL}"
  elif [[ -x "${chroot_dir}/bin/bash" ]]; then
    inner_shell="/bin/bash"
  else
    inner_shell="/bin/sh"
  fi

  echo "=> Entering chroot: ${chroot_dir}"
  export PS1="[xchroot ${chroot_dir}] ${PS1}"
  chroot "${chroot_dir}" "${cmd[@]:-${inner_shell}}"
  local status=$?
  [[ ${status} -ne 0 ]] && echo "=> Chroot exited with status: ${status}" >&2
  return ${status}
}

# System configuration
configure_system() {
  xchroot "${target_dir}" /bin/bash -s <<EOF
set -euo pipefail

xbps-pkgdb -m manual linux-base "${kernel}"

mkdir -p /etc/xbps.d
echo 'noextract=usr/share/fonts/NerdFonts/ttf/*' > /etc/xbps.d/10-nerd-fonts-cascadia.conf
echo 'noextract=!usr/share/fonts/NerdFonts/ttf/*Cascadia*' >> /etc/xbps.d/10-nerd-fonts-cascadia.conf
echo 'noextract=!usr/share/fonts/NerdFonts/ttf/symbols/*' >> /etc/xbps.d/10-nerd-fonts-cascadia.conf
echo 'noextract=usr/share/fonts/NerdFonts/otf/*' >> /etc/xbps.d/10-nerd-fonts-cascadia.conf

echo 'ignorepkg=linux' > /etc/xbps.d/99-ignore.conf
xbps-remove -Ry linux

cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
sed -i "s|https://repo-default.voidlinux.org|${xbps_repo}|g" /etc/xbps.d/*-repository-*.conf

xbps-install -Sy
xbps-install -y "${install_pkgs[@]}"
xbps-install -y void-repo-nonfree void-repo-multilib{,-nonfree}

cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
sed -i "s|https://repo-default.voidlinux.org|${xbps_repo}|g" /etc/xbps.d/*-repository-*.conf

xbps-install -Sy
xbps-install -y "${nonfree_pkgs[@]}"

echo "LANG=${localization}" > /etc/locale.conf
sed -i "/${localization}/s/^#//g" /etc/default/libc-locales
ln -sf "/usr/share/zoneinfo/${timezone}" /etc/localtime
xbps-reconfigure -f glibc-locales

echo "${hostname}" > /etc/hostname

echo '127.0.0.1 localhost' > /etc/hosts
echo "127.0.1.1 ${hostname}" >> /etc/hosts
echo '' >> /etc/hosts
echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
echo 'fe00::0 ip6-localnet' >> /etc/hosts
echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
echo 'ff02::1 ip6-allnodes' >> /etc/hosts
echo 'ff02::2 ip6-allrouters' >> /etc/hosts
echo 'ff02::3 ip6-allhosts' >> /etc/hosts

echo 'VISUAL=nvim' >> /etc/environment
echo 'EDITOR=nvim' >> /etc/environment

echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel

ln -s /etc/sv/bluetoothd /etc/runit/runsvdir/default/
ln -s /etc/sv/chronyd /etc/runit/runsvdir/default/
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/
ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/
ln -s /etc/sv/nftables /etc/runit/runsvdir/default/
ln -s /etc/sv/ngetty /etc/runit/runsvdir/default/
ln -s /etc/sv/sddm /etc/runit/runsvdir/default/
ln -s /etc/sv/snooze-weekly /etc/runit/runsvdir/default/
ln -s /etc/sv/socklog-unix /etc/runit/runsvdir/default/
ln -s /etc/sv/nanoklogd /etc/runit/runsvdir/default/
touch /etc/sv/agetty-tty{1..6}/down

mkdir -p /etc/pipewire/pipewire.conf.d
ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/

mkdir -p /etc/alsa/conf.d
ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d

echo '/usr/lib/pipewire-0.3/jack' > /etc/ld.so.conf.d/pipewire-jack.conf
ldconfig

ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/

mkdir -p /etc/NetworkManager/conf.d
echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf
echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

echo '#!/usr/bin/nft -f' > /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo 'flush ruleset' >> /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo 'table inet filter {' >> /etc/nftables.conf
echo '	chain input {' >> /etc/nftables.conf
echo '		type filter hook input priority 0;' >> /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo '		# accept any localhost traffic' >> /etc/nftables.conf
echo '		iif lo accept' >> /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo '		# accept traffic originated from us' >> /etc/nftables.conf
echo '		ct state established,related accept' >> /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo '		# activate the following line to accept common local services' >> /etc/nftables.conf
echo '		#tcp dport { 22, 80, 443 } ct state new accept' >> /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo '		# ICMPv6 packets which must not be dropped, see https://tools.ietf.org/html/rfc4890#section-4.4.1' >> /etc/nftables.conf
echo '		meta nfproto ipv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, echo-reply, echo-request, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, 148, 149 } accept' >> /etc/nftables.conf
echo '		ip6 saddr fe80::/10 icmpv6 type { 130, 131, 132, 143, 151, 152, 153 } accept' >> /etc/nftables.conf
echo '' >> /etc/nftables.conf
echo '		# count and drop any other traffic' >> /etc/nftables.conf
echo '		counter drop' >> /etc/nftables.conf
echo '	}' >> /etc/nftables.conf
echo '}' >> /etc/nftables.conf

echo '* hard core 0' >> /etc/security/limits.conf
echo '* hard nofile 1048576' >> /etc/security/limits.conf

mkdir -p /etc/sysctl.d
echo 'net.core.default_qdisc = fq_codel' > /etc/sysctl.d/tcp-congestion.conf
echo 'vm.max_map_count = 1048576' > /etc/sysctl.d/vm-maxmapcount.conf
echo 'kernel.split_lock_mitigate = 0' > /etc/sysctl.d/kernel-splitlockmitigate.conf

mkdir -p /etc/sddm.conf.d
echo '[General]' > /etc/sddm.conf.d/sddm.conf
echo 'Numlock=on' >> /etc/sddm.conf.d/sddm.conf
echo '' >> /etc/sddm.conf.d/sddm.conf
echo '[Theme]' >> /etc/sddm.conf.d/sddm.conf
echo 'Current=breeze' >> /etc/sddm.conf.d/sddm.conf

echo '[Icon Theme]' > /usr/share/icons/default/index.theme
echo 'Inherits=breeze_cursors' >> /usr/share/icons/default/index.theme

swapon --show=NAME | grep -q '/swapfile' && swapoff /swapfile
rm -f /swapfile
mkswap -U clear --size "${swap_size}" --file /swapfile
xgenfstab -U / > /etc/fstab

bootctl install
root_uuid=$(blkid -s UUID -o value "${root_partition}")
echo "CMDLINE=\"root=UUID=${root_uuid} rw ${boot_options}\"" > /etc/default/systemd-boot

mkdir -p /etc/cron.weekly
echo '#!/bin/sh' > /etc/cron.weekly/fstrim
echo 'LOG=/var/log/trim.log' >> /etc/cron.weekly/fstrim
echo 'echo "*** $(date -R) ***" >> $LOG' >> /etc/cron.weekly/fstrim
echo 'fstrim -av >> $LOG' >> /etc/cron.weekly/fstrim
chmod +x /etc/cron.weekly/fstrim

chsh -s /bin/bash > /dev/null
useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input,network,bluetooth,socklog,gamemode "${username}"
sudo -u "${username}" xdg-user-dirs-update
userpath=$(getent passwd "${username}" | cut -d: -f6)

sudo -u "${username}" mkdir -p "${userpath}/git"
cd "${userpath}/git"

if ! sudo -u "${username}" git clone --depth 1 https://gitlab.com/armond/Configurations.git; then
  echo "Failed to clone configurations!" >&2
  exit 1
fi

sudo -u "${username}" cp -r "${userpath}/git/${config_directory}/." "${userpath}/"
cp -r "${userpath}/git/${config_directory}/." ~root/

echo 'force_drivers+=" amdgpu "' > /etc/dracut.conf.d/myflags.conf
echo 'compress="lz4"' >> /etc/dracut.conf.d/myflags.conf
echo 'hostonly="yes"' >> /etc/dracut.conf.d/myflags.conf
xbps-reconfigure -f "${kernel}"

echo "${username}:${user_password}" | chpasswd
echo "root:${root_password}" | chpasswd
EOF
}

# Main installation flow
main() {
  check_root
  get_credentials

  for cmd in curl efibootmgr git tar unzip xz; do
    command -v "${cmd}" &>/dev/null || {
      echo "Required command missing: ${cmd}" >&2
      exit 1
    }
  done

  get_latest_xbps

  for bootnum in $(efibootmgr | grep '^Boot[0-9]' | cut -d' ' -f1 | sed 's/[^0-9]//g'); do
    efibootmgr -B -b "${bootnum}"
  done

  rm -f /sys/firmware/efi/efivars/dump-*

  partition_disk
  format_partitions
  mount_filesystems

  local tmpdir
  tmpdir=$(mktemp -d)
  trap 'rm -rf "${tmpdir}"' EXIT

  echo "Downloading and verifying xbps-static (${xbps_version})..."
  if ! curl -sL -o "${tmpdir}/xbps.tar.xz" "${xbps_url}"; then
    echo "ERROR: Failed to download xbps-static" >&2
    exit 1
  fi

  echo "Verifying package integrity..."
  local computed_hash
  computed_hash=$(sha256sum "${tmpdir}/xbps.tar.xz" | cut -d' ' -f1)
  if [[ "${computed_hash}" != "${xbps_hash}" ]]; then
    echo "ERROR: Checksum mismatch!" >&2
    echo "Expected: ${xbps_hash}" >&2
    echo "Actual:   ${computed_hash}" >&2
    exit 1
  fi

  if ! tar -xJf "${tmpdir}/xbps.tar.xz" -C /tmp; then
    echo "ERROR: Failed to extract xbps-static" >&2
    exit 1
  fi

  PATH="/tmp/usr/bin:${PATH}"

  install_base
  configure_system

  echo "Installation complete. Reboot when ready."
  umount -R "${target_dir}"

  unset user_password root_password
}

main "$@"
