#!/bin/bash

drive='nvme0n1'
timezone='Europe/Lisbon'
mirror='http://deb.debian.org/debian'
localization='en_US.UTF-8'
keyboardlayout='us'
hostname='pc'
swapsize='2048'
configdirectory='Configurations/home'
services='wireplumber'
install='
	linux-image-amd64 firmware-linux locales sudo curl git wget
	kde-standard wireplumber libspa-0.2-bluetooth mesa-va-drivers vdpau-driver-all nftables openresolv wireguard-tools
	flatpak htop kitty neovim xsel
	firefox audacious mpv qbittorrent telegram-desktop
'
if [ "$1" == '' ]; then
	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done

	rm -f /sys/firmware/efi/efivars/dump-*

	wipefs --force --all /dev/${drive} > /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 551MiB mkpart primary ext4 551MiB 100% set 1 esp on
	mkfs.fat -F 32 /dev/${drive}p1 > /dev/null
	mkfs.ext4 -F /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mkdir -p /mnt/boot/efi
	mount /dev/${drive}p1 /mnt/boot/efi

	pacman -Sy --needed --noconfirm archlinux-keyring debootstrap
	debootstrap sid /mnt ${mirror}

	genfstab -U /mnt >> /mnt/etc/fstab

	cp $0 /mnt/setup
	arch-chroot /mnt ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	echo 'deb http://deb.debian.org/debian sid main contrib non-free' > /etc/apt/sources.list
	echo 'deb-src http://deb.debian.org/debian sid main contrib non-free' >> /etc/apt/sources.list

	export DEBIAN_FRONTEND=noninteractive
	apt-get update
	apt-get install -y ${install}

	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	hwclock --systohc

	sed -i "/${localization}/s/^#//g" /etc/locale.gen
	locale-gen > /dev/null
	echo "LANG=${localization}" > /etc/locale.conf

	echo "KEYMAP=${keyboardlayout}" > /etc/vconsole.conf

	echo "${hostname}" > /etc/hostname

	echo '127.0.0.1 localhost' > /etc/hosts
	echo "127.0.1.1 ${hostname}" >> /etc/hosts
	echo '' >> /etc/hosts
	echo '# The following lines are desirable for IPv6 capable hosts' >> /etc/hosts
	echo '::1     ip6-localhost ip6-loopback' >> /etc/hosts
	echo 'fe00::0 ip6-localnet' >> /etc/hosts
	echo 'ff00::0 ip6-mcastprefix' >> /etc/hosts
	echo 'ff02::1 ip6-allnodes' >> /etc/hosts
	echo 'ff02::2 ip6-allrouters' >> /etc/hosts
	echo 'ff02::3 ip6-allhosts' >> /etc/hosts

	echo 'VISUAL=nvim' > /etc/environment
	echo 'EDITOR=nvim' >> /etc/environment

	sed -i '/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	systemctl enable ${services} 2> /dev/null

	echo 'Section "Device"' > /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Identifier "AMD"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Driver "amdgpu"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "SWcursor" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "Accel" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  # Option "ZaphodHeads" ""' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "DRI" "3"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "EnablePageFlip" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "TearFree" "on"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "VariableRefresh" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "AsyncFlipSecondaries" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "AccelMethod" "glamor"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo '  Option "ShadowPrimary" "off"' >> /etc/X11/xorg.conf.d/20-amdgpu.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/20-amdgpu.conf

	echo 'Section "InputClass"' > /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Identifier "touchpad"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Driver "libinput"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  MatchIsTouchpad "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Option "Tapping" "on"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo '  Option "NaturalScrolling" "true"' >> /etc/X11/xorg.conf.d/30-touchpad.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/30-touchpad.conf

	echo '[main]' > /etc/NetworkManager/conf.d/rc-manager.conf
	echo 'rc-manager=resolvconf' >> /etc/NetworkManager/conf.d/rc-manager.conf

	echo '[keyfile]' > /etc/NetworkManager/conf.d/unmanaged.conf
	echo 'unmanaged-devices=type:wireguard' >> /etc/NetworkManager/conf.d/unmanaged.conf

	echo '#!/usr/bin/nft -f' > /etc/nftables.conf
	echo '# vim:set ts=2 sw=2 et:' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '# IPv4/IPv6 Simple & Safe firewall ruleset.' >> /etc/nftables.conf
	echo '# More examples in /usr/share/nftables/ and /usr/share/doc/nftables/examples/.' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo 'table inet filter' >> /etc/nftables.conf
	echo 'delete table inet filter' >> /etc/nftables.conf
	echo 'table inet filter {' >> /etc/nftables.conf
	echo '  chain input {' >> /etc/nftables.conf
	echo '    type filter hook input priority filter' >> /etc/nftables.conf
	echo '    policy drop' >> /etc/nftables.conf
	echo '' >> /etc/nftables.conf
	echo '    ct state invalid drop comment "early drop of invalid connections"' >> /etc/nftables.conf
	echo '    ct state {established, related} accept comment "allow tracked connections"' >> /etc/nftables.conf
	echo '    iifname lo accept comment "allow from loopback"' >> /etc/nftables.conf
	echo '    ip protocol icmp accept comment "allow icmp"' >> /etc/nftables.conf
	echo '    meta l4proto ipv6-icmp accept comment "allow icmp v6"' >> /etc/nftables.conf
	echo '    tcp dport ssh accept comment "allow sshd"' >> /etc/nftables.conf
	echo '    pkttype host limit rate 5/second counter reject with icmpx type admin-prohibited' >> /etc/nftables.conf
	echo '    counter' >> /etc/nftables.conf
	echo '  }' >> /etc/nftables.conf
	echo '  chain forward {' >> /etc/nftables.conf
	echo '    type filter hook forward priority filter' >> /etc/nftables.conf
	echo '    policy drop' >> /etc/nftables.conf
	echo '  }' >> /etc/nftables.conf
	echo '}' >> /etc/nftables.conf

	mkdir -p /etc/sddm.conf.d
	echo '[General]' > /etc/sddm.conf.d/sddm.conf
	echo 'Numlock=on' >> /etc/sddm.conf.d/sddm.conf
	echo '' >> /etc/sddm.conf.d/sddm.conf
	echo '[Theme]' >> /etc/sddm.conf.d/sddm.conf
	echo 'Current=breeze' >> /etc/sddm.conf.d/sddm.conf
	echo 'CursorTheme=Breeze_Snow' >> /etc/sddm.conf.d/sddm.conf

	echo '[General]' > /usr/share/sddm/themes/breeze/theme.conf.user
	echo 'background=/usr/share/wallpapers/MilkyWay/contents/images/5120x2880.png' >> /usr/share/sddm/themes/breeze/theme.conf.user

	echo '* hard nofile 1048576' > /etc/security/limits.conf

	echo 'kernel.core_pattern=/dev/null' > /etc/sysctl.d/50-coredump.conf

	sed -i 's/#SystemMaxUse=/SystemMaxUse=50M/' /etc/systemd/journald.conf

	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 2> /dev/null
	chmod 600 /swapfile
	mkswap /swapfile > /dev/null
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab

	uuid=$(blkid -o value -s UUID /dev/${drive}p2)
	echo "fs0:\vmlinuz root=${uuid} initrd=initrd.img" > /mnt/boot/efi/startup.nsh

	cp -f /vmlinuz /initrd.img /boot/efi
	echo '#!/bin/sh' > /etc/kernel/postinst.d/zz-update-efistub
	echo 'cp -f /vmlinuz /initrd.img /boot/efi' >> /etc/kernel/postinst.d/zz-update-efistub
	chmod +x /etc/kernel/postinst.d/zz-update-efistub

	useradd -m -G sudo -s /bin/bash ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	sudo -u ${user} mkdir -p ${userpath}/git
	cd ${userpath}/git
	sudo -u ${user} git clone --depth 1 https://gitlab.com/armond/Configurations.git
	sudo -u ${user} cp -R ${userpath}/git/${configdirectory}/. ${userpath}/
	cp -R ${userpath}/git/${configdirectory}/. ~root/
	sudo -u ${user} curl -LO https://download.damieng.com/fonts/original/EnvyCodeR-PR7.zip
	sudo -u ${user} unzip EnvyCodeR-PR7 > /dev/null
	sudo -u ${user} mkdir -p ${userpath}/.local/share/fonts/'Envy Code R'
	sudo -u ${user} cp -R ${userpath}/git/'Envy Code R PR7'/*.ttf ${userpath}/.local/share/fonts/'Envy Code R'/
	find ${userpath}/git -delete

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null

	apt-get clean
fi
