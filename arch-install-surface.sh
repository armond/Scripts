#!/bin/bash

drive='sda'
timezone='Europe/Lisbon'
localization='en_US.UTF-8'
keyboardlayout='us'
hostname='surface'
swapsize='4096M'
bootoptions='mitigations=off nmi_watchdog=0 quiet'
configdirectory='Configurations/home'
services='bluetooth NetworkManager org.cups.cupsd'
install='
	base base-devel linux linux-firmware intel-ucode efibootmgr git grml-zsh-config zsh-autosuggestions zsh-syntax-highlighting pbzip2 pigz wget xdg-user-dirs
	xorg-server xorg-xinit xf86-video-intel libva-intel-driver
	plasma-desktop kscreen plasma-nm plasma-pa powerdevil bluedevil pulseaudio-bluetooth breeze-gtk
	alacritty ranger xarchiver p7zip zip unzip unrar ntfs-3g sxiv howl zathura-pdf-mupdf
	firefox htop mpv neofetch transmission-cli weechat youtube-dl cups-pdf hplip system-config-printer simple-scan
	noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
'
aurinstall='
	https://aur.archlinux.org/yay.git
'
gitdownload='
	https://gitlab.com/armond/Configurations.git
'
if [ "$1" == '' ]; then
	loadkeys ${keyboardlayout}

	echo 'Enter user name'
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo 'Enter root password'
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	dd if=/dev/zero of=/dev/${drive} bs=1M count=40 2> /dev/null
	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 1MiB 257MiB mkpart primary btrfs 257MiB 100% set 1 esp on
	mkfs.fat -F32 /dev/${drive}1 > /dev/null
	mkfs.btrfs -f /dev/${drive}2 > /dev/null 2> /dev/null
	mount /dev/${drive}2 /mnt
	mkdir -p /mnt/boot
	mount /dev/${drive}1 /mnt/boot

	echo 'Server = https://ftp.rnl.tecnico.ulisboa.pt/pub/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

	pacstrap /mnt ${install}

	genfstab -U /mnt >> /mnt/etc/fstab

	cp $0 /mnt/setup
	arch-chroot /mnt ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	hwclock --systohc

	sed -i "/${localization}/s/^#//g" /etc/locale.gen
	echo "LANG=${localization}" > /etc/locale.conf
	locale-gen > /dev/null

	echo "KEYMAP=${keyboardlayout}" > /etc/vconsole.conf

	echo "${hostname}" > /etc/hostname
	echo '127.0.0.1  localhost' > /etc/hosts
	echo '::1        localhost' >> /etc/hosts
	echo "127.0.1.1  ${hostname}.localdomain  ${hostname}" >> /etc/hosts

	echo 'VISUAL=howl' >> /etc/environment

	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^# //g' /etc/sudoers

	echo 'Section "Device"' > /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Identifier "Intel Graphics"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Driver "intel"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "ColorKey" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "DRI" "3"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "Accel" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "Present" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "AccelMethod" "sna"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "TearFree" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "ReprobeOutputs" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "VideoKey" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "XvPreferOverlay" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "Backlight" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "CustomEDID" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "FallbackDebug" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "DebugFlushBatches" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "DebugFlushCaches" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "DebugWait" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "HWRotation" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "VSync" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "PageFlip" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "SwapbuffersWait" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "TripleBuffer" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "Tiling" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "LinearFramebuffer" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "RelaxedFencing" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "XvMC" "off"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "Throttle" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "HotPlug" "on"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  Option "Virtualheads" "0"' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo '  # Option "ZaphodHeads" ""' >> /etc/X11/xorg.conf.d/20-intel.conf
	echo 'EndSection' >> /etc/X11/xorg.conf.d/20-intel.conf

	truncate -s 0 /swapfile
	chattr +C /swapfile
	fallocate -l ${swapsize} /swapfile
	chmod 600 /swapfile
	mkswap /swapfile > /dev/null
	echo '# /swapfile' >> /etc/fstab
	echo '/swapfile    none    swap    defaults    0 0' >> /etc/fstab

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done
	partuuid=$(blkid -o value -s PARTUUID /dev/${drive}2)
	efibootmgr -c -g -d /dev/${drive} -p 1 -L 'Arch Linux' -l /vmlinuz-linux -u "root=PARTUUID=${partuuid} rw ${bootoptions} initrd=/intel-ucode.img initrd=/initramfs-linux.img" > /dev/null

	chsh -s /bin/zsh > /dev/null
	useradd -m -G wheel -s /bin/zsh ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	systemctl enable ${services} 2> /dev/null

	mkdir -p /media
	echo 'ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"' > /etc/udev/rules.d/99-udisks2.rules
	echo 'D /media 0755 root root 0 -' > /etc/tmpfiles.d/media.conf

	echo 'kernel.dmesg_restrict = 1' > /etc/sysctl.d/50-dmesg-restrict.conf

	sed -i -e 's/base udev autodetect modconf block filesystems keyboard fsck/systemd autodetect modconf block filesystems keyboard/' -e '/COMPRESSION="lz4"/s/^#//g' -e 's/#COMPRESSION_OPTIONS=()/COMPRESSION_OPTIONS=(-9)/' /etc/mkinitcpio.conf

	sed -i -e 's/-march=x86-64 -mtune=generic/-march=native/' -e '/MAKEFLAGS=/s/^#//g;s/-j2/-j$(nproc)/' -e 's/gzip -c -f -n/pigz -c -f -n/' -e 's/bzip2 -c -f/pbzip2 -c -f/' -e 's/xz -c -z -/xz -c -z - --threads=0/' /etc/makepkg.conf

	sed -i 's/#SystemMaxUse=/SystemMaxUse=50M/' /etc/systemd/journald.conf

	sed -i 's/#Storage=external/Storage=none/' /etc/systemd/coredump.conf

	sed -i '/Color/s/^#//g' /etc/pacman.conf

	for item in ${aurinstall}; do
		name=$(basename ${item} .git)
		echo "Installing ${name}"
		cd /tmp
		sudo -u ${user} git clone ${item} 2> /dev/null
		cd ${name}
		sudo -u ${user} makepkg -sirc --noconfirm
		cd /tmp
		rm -rf ${name}
	done

	for item in ${gitdownload}; do
		name=$(basename ${item} .git)
		echo "Downloading ${name}"
		cd ${userpath}/Documents
		sudo -u ${user} git clone ${item} 2> /dev/null
	done

	echo 'Downloading Envy Code R'
	sudo -u ${user} curl -LO https://download.damieng.com/fonts/original/EnvyCodeR-PR7.zip
	sudo -u ${user} unzip EnvyCodeR-PR7 > /dev/null
	sudo -u ${user} mkdir -p ${userpath}/.local/share/fonts/'Envy Code R'
	cp -a ${userpath}/Documents/'Envy Code R PR7'/*.ttf ${userpath}/.local/share/fonts/'Envy Code R'/

	if [ "$configdirectory" != '' ]; then
		cp -a ${userpath}/Documents/${configdirectory}/. ${userpath}/
		cp -dR ${userpath}/Documents/${configdirectory}/. /root/
		rm -rf ${userpath}/Documents/{*,.*} 2> /dev/null
	fi

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null

	mkinitcpio -P
fi
